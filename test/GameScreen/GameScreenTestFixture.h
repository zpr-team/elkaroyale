//
// Created by Radoslaw Panuszewski on 12/6/18.
//

#ifndef ELKAROYALE_GAMESCREENTESTFIXTURE_H
#define ELKAROYALE_GAMESCREENTESTFIXTURE_H

#include <gtest/gtest.h>
#include <fakeit.hpp>
#include <render_windows/MockableRenderWindow.h>
#include <views/player/PlayerView.h>
#include <views/screens/game/GameScreen.h>
#include "../helper/RenderWindowMockHelper.h"

using namespace fakeit;

class GameScreenTestFixture: public ::testing::Test {

protected:
    ///GameScreen dependencies
    Mock<GameScreenViewModel> gameScreenViewModel;
    Mock<Keyboard> keyboard;
    Mock<Session> session;
    PlayerViewFactory playerViewFactory;
    BulletViewFactory bulletVMFactory;

    ///mocked single instances of PlayerView and PlayerViewModel to provide whenever one of those is needed
    Mock<PlayerView> playerView;
    Mock<PlayerViewModel> playerViewModel;

    ///for the draw() call
    ///note: we cannot simply do Mock<MockableRenderWindow> because FakeIt does not support multiple inheritance
    ///see documentation for RenderWindowMockHelper for more details
    Mock<RenderWindowMockInterface> renderWindow;


    static void SetUpTestCase() {
        renderWindowMockHelper = std::make_unique<RenderWindowMockHelper>();
    }

    RenderWindowMockHelper *get(Mock<RenderWindowMockInterface> &renderWindowMock) {
        renderWindowMockHelper->setMockInterface(&renderWindowMock.get());
        return renderWindowMockHelper.get();
    }
//    void letHasPressedKeys(Mock<Keyboard> &keyboard, std::initializer_list<sf::Keyboard::Key> keyCodes) {
//        When(Method(keyboard, isKeyPressed)).AlwaysReturn(false);
//
//        for (sf::Keyboard::Key keyCode : keyCodes)
//            When(Method(keyboard, isKeyPressed).Using(keyCode)).Return(true);
//    }

    static std::unique_ptr<RenderWindowMockHelper> renderWindowMockHelper;
};


#endif //ELKAROYALE_GAMESCREENTESTFIXTURE_H
