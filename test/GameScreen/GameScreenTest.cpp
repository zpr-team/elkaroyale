//
// Created by Radoslaw Panuszewski on 12/6/18.
//

#include <gtest/gtest.h>
#include <fakeit.hpp>

#include <views/screens/game/GameScreen.h>
#include "../helper/RenderWindowMockHelper.h"
#include "GameScreenTestFixture.h"

using namespace fakeit;


TEST_F(GameScreenTestFixture, GivenNoPlayers_WhenDrawScreen_ThenRenderWindowIsClearedAndDisplayed) {

    ///===================== MOCKING ==============================
    Fake(Method(gameScreenViewModel, listenOnPlayerSpawned));
    Fake(Method(gameScreenViewModel, listenOnBulletSpawned));
    Fake(Method(gameScreenViewModel, listenOnPlayerDestroyed));

    Fake(Method(renderWindow, clear));
    Fake(Method(renderWindow, display));

    //we don't need to mock that, since the GameScreen won't create any PlayerView's in this test case
    playerViewFactory = []() { return nullptr; };

    ///===================== TESTING ==============================
    GameScreen gameScreen(
            &gameScreenViewModel.get(),
            &keyboard.get(),
            &session.get(),
            playerViewFactory,
            bulletVMFactory
    );

    gameScreen.draw(get(renderWindow));

    Verify(Method(renderWindow, clear)).Once();
    Verify(Method(renderWindow, display)).Once();
}

/**
 * we are going to imitate GameScreenViewModel behavior, i.e.
 * to capture the listener registered by GameScreen and then to
 * notify it with our mocked PlayerViewModel, so it will
 * create PlayerView for that
 */
TEST_F(GameScreenTestFixture, GivenThreePlayers_WhenCallDraw_ThenAllThreePlayerViewsCallDraw) {

    ///===================== MOCKING ==============================
    Fake(Method(playerView, setViewModel));
    Fake(Method(playerView, draw));

    //provide subsequent IDs for the players
    When(Method(playerViewModel, getId)).Return(0).Return(1).Return(2);

    //capture the observer to notify it later
    PlayerVMConsumer observer;
    When(Method(gameScreenViewModel, listenOnPlayerSpawned)).Do([&](PlayerVMConsumer _observer) { observer = _observer; });
    Fake(Method(gameScreenViewModel, listenOnBulletSpawned));
    Fake(Method(gameScreenViewModel, listenOnPlayerDestroyed));

    Fake(Method(renderWindow, clear));
    Fake(Method(renderWindow, display));

    //provide the same mocked PlayerView instance each time
    playerViewFactory = [&]() { return std::unique_ptr<PlayerView>(&playerView.get()); };

    ///===================== TESTING ==============================
    GameScreen gameScreen(
            &gameScreenViewModel.get(),
            &keyboard.get(),
            &session.get(),
            playerViewFactory,
            bulletVMFactory
    );

    //add three players (actually pass 3 times the mocked instance)
    observer(&playerViewModel.get());
    observer(&playerViewModel.get());
    observer(&playerViewModel.get());

    gameScreen.draw(get(renderWindow));

    Verify(Method(playerView, draw).Using(get(renderWindow))).Exactly(3);
}

/**
 * we are going to imitate GameScreenViewModel behavior, i.e.
 * to capture the listener registered by GameScreen and then to
 * notify it with our mocked PlayerViewModel, so it will
 * create PlayerView for that
 */
TEST_F(GameScreenTestFixture, GivenTwoPlayers_WhenCallUpdate_ThenOnlyLocalPlayerCallsUpdate) {

    ///===================== MOCKING ==============================
    Fake(Method(playerView, setViewModel));
    Fake(Method(playerView, update));

    //provide subsequent IDs for the players
    When(Method(playerViewModel, getId)).Return(0).Return(1);

    //capture the observer to notify it later
    PlayerVMConsumer observer;
    When(Method(gameScreenViewModel, listenOnPlayerSpawned)).Do([&](PlayerVMConsumer _observer) { observer = _observer; });
    Fake(Method(gameScreenViewModel, listenOnBulletSpawned));
    Fake(Method(gameScreenViewModel, listenOnPlayerDestroyed));

    When(Method(session, getLocalPlayerId)).AlwaysReturn(0);

    //provide same mocked PlayerView instance each time
    playerViewFactory = [&]() { return std::unique_ptr<PlayerView>(&playerView.get()); };

    ///===================== TESTING ==============================
    GameScreen gameScreen(
            &gameScreenViewModel.get(),
            &keyboard.get(),
            &session.get(),
            playerViewFactory,
            bulletVMFactory
    );

    //add two players (actually pass 2 times the mocked instance)
    observer(&playerViewModel.get());
    observer(&playerViewModel.get());

    gameScreen.update();

    Verify(Method(playerView, update)).Exactly(1);
}