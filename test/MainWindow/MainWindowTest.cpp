//
// Created by Radoslaw Panuszewski on 12/5/18.
//

#include <gtest/gtest.h>
#include <windows/MainWindow.h>
#include "MainWindowTestFixture.h"

using namespace fakeit;


TEST_F(MainWindowTestFixture, WhenWindowCallsShow_ThenScreenCallsDrawAndUpdate) {

    ///===================== MOCKING ==============================
    Fake(Method(screen, draw));
    Fake(Method(screen, update));

    When(Method(renderWindow, isOpen)).Return(true).Return(false);

    When(Method(renderWindow, hasFocus)).AlwaysReturn(true);

    Fake(Method(renderWindow, pollEvent));

    ///===================== TESTING ==============================
    MainWindow mainWindow(get(renderWindow), &screen.get());
    mainWindow.show();

    Verify(Method(screen, draw)).Once();
    Verify(Method(screen, update)).Once();
}

TEST_F(MainWindowTestFixture, WhenWindowGeneratesEvent_ThenThatEventIsPropagated) {

    ///===================== MOCKING ==============================
    Fake(Method(screen, draw));
    Fake(Method(screen, update));
    Fake(Method(screen, onSFMLEvent));

    When(Method(renderWindow, isOpen)).Return(true).Return(false);

    When(Method(renderWindow, hasFocus)).AlwaysReturn(true);

    When(Method(renderWindow, pollEvent))
            .Do([=](sf::Event& event) {
                event = keyPressedEvent(sf::Keyboard::Left);
                return true;
            })
            .Return(false);

    ///===================== TESTING ==============================
    MainWindow mainWindow(get(renderWindow), &screen.get());
    mainWindow.show();

    Verify(Method(screen, draw)).Once();
    Verify(Method(screen, update)).Once();
    Verify(Method(screen, onSFMLEvent).Using(keyPressedEvent(sf::Keyboard::Left))).Once();
}

TEST_F(MainWindowTestFixture, WhenEventClosed_ThanWindowCloses) {

    ///===================== MOCKING ==============================
    Fake(Method(screen, draw));
    Fake(Method(screen, update));
    Fake(Method(screen, onSFMLEvent));

    When(Method(renderWindow, isOpen)).Return(true).Return(false);

    When(Method(renderWindow, hasFocus)).AlwaysReturn(true);

    When(Method(renderWindow, pollEvent))
            .Do([=](sf::Event& event) {
                event = eventOfType(sf::Event::Closed);
                return true;
            })
            .Return(false);

    Fake(Method(renderWindow, close));

    ///===================== TESTING ==============================
    MainWindow mainWindow(get(renderWindow), &screen.get());
    mainWindow.show();

    Verify(Method(renderWindow, close)).Once();
    Verify(Method(screen, draw)).Once();
    Verify(Method(screen, update)).Once();
}