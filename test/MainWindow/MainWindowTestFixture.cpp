//
// Created by Radoslaw Panuszewski on 12/6/18.
//

#include "MainWindowTestFixture.h"

bool operator==(const sf::Event& left, const sf::Event& right) {

    if (left.type != right.type)
        return false;

    if (left.type == sf::Event::KeyPressed)
        return left.key.code == right.key.code;
    
}

std::unique_ptr<RenderWindowMockHelper> MainWindowTestFixture::renderWindowMockHelper;