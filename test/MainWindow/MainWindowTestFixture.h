//
// Created by Radoslaw Panuszewski on 12/6/18.
//

#ifndef ELKAROYALE_RENDERWINDOWFIXTURE_H
#define ELKAROYALE_RENDERWINDOWFIXTURE_H

#include <gtest/gtest.h>
#include <SFML/Window/Event.hpp>
#include <fakeit.hpp>
#include <render_windows/MockableRenderWindow.h>
#include <views/Screen.h>
#include "../helper/RenderWindowMockHelper.h"

using namespace fakeit;

class MainWindowTestFixture: public ::testing::Test {

protected:

    ///MainWindow dependency
    Mock<Screen> screen;

    ///for the draw() call
    ///note: we cannot simply do Mock<MockableRenderWindow> because FakeIt does not support multiple inheritance
    ///see documentation for RenderWindowMockHelper for more details
    Mock<RenderWindowMockInterface> renderWindow;

    static void SetUpTestCase() {
        renderWindowMockHelper = std::make_unique<RenderWindowMockHelper>();
    }

    MockableRenderWindow *get(Mock<RenderWindowMockInterface> &renderWindowMock) {
        renderWindowMockHelper->setMockInterface(&renderWindowMock.get());
        return renderWindowMockHelper.get();
    }

    sf::Event eventOfType(sf::Event::EventType type) {
        sf::Event event {};
        event.type = type;
        return event;
    }

    sf::Event keyPressedEvent(sf::Keyboard::Key keyCode) {
        return keyEvent(sf::Event::KeyPressed, keyCode);
    }

    sf::Event keyReleasedEvent(sf::Keyboard::Key keyCode) {
        return keyEvent(sf::Event::KeyReleased, keyCode);
    }

    sf::Event mouseButtonPressedEvent(sf::Mouse::Button button) {
        return mouseButtonEvent(sf::Event::MouseButtonPressed, button);
    }

    sf::Event mouseButtonReleasedEvent(sf::Mouse::Button button) {
        return mouseButtonEvent(sf::Event::MouseButtonReleased, button);
    }

private:
    sf::Event keyEvent(sf::Event::EventType type, sf::Keyboard::Key keyCode) {
        sf::Event event {};
        event.type = type;
        event.key.code = keyCode;
        return event;
    }

    sf::Event mouseButtonEvent(sf::Event::EventType type, sf::Mouse::Button button) {
        sf::Event event {};
        event.type = type;
        event.mouseButton.button = button;
        return event;
    }

protected:
    static std::unique_ptr<RenderWindowMockHelper> renderWindowMockHelper;
};


bool operator==(const sf::Event& left, const sf::Event& right);


#endif //ELKAROYALE_RENDERWINDOWFIXTURE_H
