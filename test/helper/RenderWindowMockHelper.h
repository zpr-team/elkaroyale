//
// Created by Radoslaw Panuszewski on 12/6/18.
//

#ifndef ELKAROYALE_RENDERWINDOWMOCKHELPER_H
#define ELKAROYALE_RENDERWINDOWMOCKHELPER_H


#include "render_windows/MockableRenderWindow.h"

/**
 * Helper class that connects MockableRenderWindow (and so the sf::RenderWindow hierarchy) and the RenderWindowMockInterface
 * NOTE: We can make Mock<RenderWindowMockInterface> but not Mock<MockableRenderWindow>, because the second one
 * inherits from sf::RenderWindow which has multiple base classes (and FakeIt does not support it)
 * USAGE:
 *  1. Mock<RenderWindowMockInterface> mock;
 *  2. When(Method(mock, ...).Do(...)
 *  3. RenderWindowMockHelper helper;
 *  4. helper.setMockInterface(&mock.get());
 *  5. Use helper as MockableRenderWindow instance
 */
class RenderWindowMockHelper: public MockableRenderWindow {
public:
    explicit RenderWindowMockHelper(RenderWindowMockInterface *mockInterface): mockInterface(mockInterface) {}

    RenderWindowMockHelper(): mockInterface(nullptr) {}

    bool isOpen() const override {
        return mockInterface->isOpen();
    }

    bool pollEvent(sf::Event &event) override {
        return mockInterface->pollEvent(event);
    }

    void clear(const sf::Color &color) override {
        mockInterface->clear(color);
    }

    void display() override {
        mockInterface->display();
    }

    void close() override {
        mockInterface->close();
    }

    bool hasFocus() override {
        return mockInterface->hasFocus();
    }

    void setMockInterface(RenderWindowMockInterface *mockInterface) {
        this->mockInterface = mockInterface;
    }
private:
    RenderWindowMockInterface *mockInterface;
};


#endif //ELKAROYALE_RENDERWINDOWMOCKHELPER_H
