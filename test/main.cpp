//
// Created by Radoslaw Panuszewski on 12/5/18.
//

#include <gmock/gmock.h>

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}