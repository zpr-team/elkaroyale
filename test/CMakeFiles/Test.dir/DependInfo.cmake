# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/radek/Desktop/ElkaRoyale/test/GameScreen/GameScreenTest.cpp" "/home/radek/Desktop/ElkaRoyale/test/CMakeFiles/Test.dir/GameScreen/GameScreenTest.cpp.o"
  "/home/radek/Desktop/ElkaRoyale/test/GameScreen/GameScreenTestFixture.cpp" "/home/radek/Desktop/ElkaRoyale/test/CMakeFiles/Test.dir/GameScreen/GameScreenTestFixture.cpp.o"
  "/home/radek/Desktop/ElkaRoyale/test/MainWindow/MainWindowTest.cpp" "/home/radek/Desktop/ElkaRoyale/test/CMakeFiles/Test.dir/MainWindow/MainWindowTest.cpp.o"
  "/home/radek/Desktop/ElkaRoyale/test/MainWindow/MainWindowTestFixture.cpp" "/home/radek/Desktop/ElkaRoyale/test/CMakeFiles/Test.dir/MainWindow/MainWindowTestFixture.cpp.o"
  "/home/radek/Desktop/ElkaRoyale/test/main.cpp" "/home/radek/Desktop/ElkaRoyale/test/CMakeFiles/Test.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "test/../src"
  "test/../lib/fake-it/gtest"
  "test/../lib/googletest-master/googletest/include"
  "test/../lib/googletest-master/googlemock/include"
  "lib/googletest-master/googletest/include"
  "lib/googletest-master/googletest"
  "lib/googletest-master/googlemock/include"
  "lib/googletest-master/googlemock"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/radek/Desktop/ElkaRoyale/src/CMakeFiles/ElkaRoyaleLib.dir/DependInfo.cmake"
  "/home/radek/Desktop/ElkaRoyale/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/radek/Desktop/ElkaRoyale/lib/googletest-master/googlemock/gtest/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/radek/Desktop/ElkaRoyale/lib/googletest-master/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  "/home/radek/Desktop/ElkaRoyale/lib/googletest-master/googlemock/CMakeFiles/gmock_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
