//
// Created by Radoslaw Panuszewski on 11/20/18.
//

#ifndef ELKAROYALE_WINDOW_H
#define ELKAROYALE_WINDOW_H

class Screen;

#include <render_windows/MockableRenderWindow.h>
#include "views/Screen.h"

class Window {

public:
    virtual void show() = 0;
};


#endif //ELKAROYALE_WINDOW_H
