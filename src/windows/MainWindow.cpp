//
// Created by Radoslaw Panuszewski on 11/16/18.
//

#include <SFML/Window/VideoMode.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Window/Event.hpp>
#include "MainWindow.h"
#include "views/screens/proxy/CurrentScreenProxy.h"
#include "../util/system_info/LinuxSystemInfoProvider.h"

MainWindow::MainWindow(MockableRenderWindow* window, Screen *currentScreen)
    : renderWindow(window),
      currentScreen(currentScreen),
      view(sf::FloatRect(0.f, 0.f, window->getSize().x, window->getSize().y)) {

    window->setFramerateLimit(60);
    window->setVerticalSyncEnabled(true);
    window->setView(view);
}

void MainWindow::show() {

    sf::Clock clock;

    while (renderWindow->isOpen()) {

        sf::Event event {};

        while (renderWindow->pollEvent(event)) {

            if (renderWindow->hasFocus())
                currentScreen->onSFMLEvent(event);

            if (event.type == sf::Event::Closed ||
                (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
            {
                renderWindow->close();
            }

            if (event.type == sf::Event::KeyPressed) {

                switch(event.key.code) {

                    case sf::Keyboard::Left:
                        view.move(-50.f, 0.f);
                        renderWindow->setView(view);
                        break;

                    case sf::Keyboard::Right:
                        view.move(50.f, 0.f);
                        renderWindow->setView(view);
                        break;

                    case sf::Keyboard::Up:
                        view.move(0.f, -50.f);
                        renderWindow->setView(view);
                        break;

                    case sf::Keyboard::Down:
                        view.move(0.f, 50.f);
                        renderWindow->setView(view);
                        break;
                }
            }
        }
        if (renderWindow->hasFocus())
            currentScreen->update();

        currentScreen->draw(renderWindow);
    }
}

fruit::Component<Window> getMainWindowComponent() {
    return fruit::createComponent()
            .bind<Window, MainWindow>()
            .bind<Screen, CurrentScreenProxy>()
            .install(getScreenComponent)
            .bind<MockableRenderWindow, MainRenderWindow>()
#ifdef __linux__
            .bind<SystemInfoProvider, LinuxSystemInfoProvider>();
#elif _WIN32
            .bind<SystemInfoProvider, WindowsSystemInfoProvider>();
#endif

}