//
// Created by Radoslaw Panuszewski on 11/16/18.
//

#ifndef ELKAROYALE_GAME_H
#define ELKAROYALE_GAME_H

#include <memory>
#include <mutex>
#include <SFML/Graphics.hpp>
#include <fruit/fruit.h>

#include "views/Screen.h"
#include "Window.h"
#include "../util/system_info/SystemInfoProvider.h"
#include "../render_windows/MockableRenderWindow.h"
#include "../render_windows/MainRenderWindow.h"


class MainWindow: public Window {
public:
    INJECT(MainWindow(
            MockableRenderWindow* renderWindow,
            Screen *activeScreen));

public:
    void show() override;

private:
    Screen *currentScreen;
    MockableRenderWindow* renderWindow;
    sf::View view;
};

fruit::Component<Window> getMainWindowComponent();

#endif //ELKAROYALE_GAME_H
