//
// Created by radek on 1/10/19.
//

#include <SFML/Window/Event.hpp>
#include "BulletView.h"

BulletView::BulletView() : bullet(5.f) {

    bullet.setFillColor(sf::Color::Black);
    bullet.setOrigin(bullet.getRadius(), bullet.getRadius());
}

void BulletView::draw(MockableRenderWindow *window) {

    bullet.setPosition(viewModel->getPosition());
    window->draw(bullet);
}

void BulletView::update() {
    viewModel->continueFlight();
}

void BulletView::onSFMLEvent(sf::Event event) {

}
