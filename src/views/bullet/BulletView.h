//
// Created by radek on 1/10/19.
//

#ifndef ELKAROYALE_BULLETVIEW_H
#define ELKAROYALE_BULLETVIEW_H


#include <views/View.h>
#include <SFML/Graphics/RectangleShape.hpp>
#include <views/EntityView.h>
#include <view_models/bullet/BulletViewModel.h>
#include <SFML/Graphics/CircleShape.hpp>

class BulletView: public EntityView<BulletViewModel> {

public:
    INJECT(BulletView());

    void draw(MockableRenderWindow *window) override;

    void update() override;

    void onSFMLEvent(sf::Event event) override;

private:
    sf::CircleShape bullet;
};

using BulletViewFactory = std::function<std::unique_ptr<BulletView>()>;


#endif //ELKAROYALE_BULLETVIEW_H
