//
// Created by radek on 1/10/19.
//

#ifndef ELKAROYALE_ENTITYVIEW_H
#define ELKAROYALE_ENTITYVIEW_H


#include "View.h"

template <typename TViewModel>
class EntityView: public View {

public:
    virtual void setViewModel(TViewModel *viewModel) {
        this->viewModel = viewModel;
    }

protected:
    TViewModel *viewModel;
};


#endif //ELKAROYALE_ENTITYVIEW_H
