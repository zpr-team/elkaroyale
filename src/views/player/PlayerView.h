//
// Created by Radoslaw Panuszewski on 12/1/18.
//

#ifndef ELKAROYALE_PLAYERDRAWABLE_H
#define ELKAROYALE_PLAYERDRAWABLE_H

#include <SFML/Graphics.hpp>
#include <util/Keyboard.h>
#include <util/Mouse.h>
#include <views/EntityView.h>
#include "views/View.h"
#include "view_models/player/PlayerViewModel.h"

class PlayerView: public EntityView<PlayerViewModel> {
public:
    INJECT(PlayerView(Keyboard *keyboard, Mouse *mouse, MockableRenderWindow *renderWindow));

    virtual ~PlayerView() = default;

    virtual void draw(MockableRenderWindow*  window) override;

    virtual void update() override;

    void onSFMLEvent(sf::Event event) override;

    void setViewModel(PlayerViewModel *viewModel) override;

private:
    float calculateRotation();
    sf::Vector2f calculateLookingDir();
    void drawOccupiedCells(MockableRenderWindow *) const;
    bool isDead() { return viewModel->getHealthPoints() <= 0; }
    void drawHitbox(MockableRenderWindow *window) const;

    sf::CircleShape body;
    sf::RectangleShape gun;
    sf::CircleShape hpIndicator;
    sf::Font font;
    sf::Text nickname;

    Keyboard *keyboard;
    Mouse *mouse;
    MockableRenderWindow *renderWindow;
};

using PlayerViewFactory = std::function<std::unique_ptr<PlayerView>()>;


#endif //ELKAROYALE_PLAYERDRAWABLE_H