//
// Created by Radoslaw Panuszewski on 12/1/18.
//

#include <cmath>
#include <util/maths.h>
#include "PlayerView.h"
#include <SFML/Main.hpp>

PlayerView::PlayerView(Keyboard *keyboard, Mouse *mouse, MockableRenderWindow *renderWindow)
        : body(50.f),
          gun(sf::Vector2f(20.f, 50.f)),
          hpIndicator(30.f),
          keyboard(keyboard),
          mouse(mouse),
          renderWindow(renderWindow) {

    body.setOrigin(body.getRadius(), body.getRadius());
    gun.setOrigin(gun.getSize().x / 2, gun.getSize().y + body.getRadius() - 2.f);

    hpIndicator.setOrigin(hpIndicator.getRadius(), hpIndicator.getRadius());

    font.loadFromFile("6809_chargen.ttf");
    nickname.setFont(font);
    nickname.setFillColor(sf::Color::Black);
    nickname.setCharacterSize(20);
}

void PlayerView::setViewModel(PlayerViewModel *viewModel) {
    EntityView::setViewModel(viewModel);

    body.setFillColor(viewModel->getColor());
    gun.setFillColor(viewModel->getColor());
    hpIndicator.setFillColor(sf::Color(viewModel->getColor() + sf::Color(70, 70, 70)));

    viewModel->listenOnGotHit([=](sf::Int64 shooterId) {
        sf::Color normalColor = viewModel->getColor();
        body.setFillColor(sf::Color(normalColor.r + 20, normalColor.g + 20, normalColor.b + 20));
        gun.setFillColor(sf::Color(normalColor.r + 20, normalColor.g + 20, normalColor.b + 20));

        std::thread([=] {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            body.setFillColor(viewModel->getColor());
            gun.setFillColor(viewModel->getColor());
        }).detach();
    });
}

void PlayerView::draw(MockableRenderWindow* window)
{
#ifdef DEBUG_OCCUPIED_CELLS
    drawOccupiedCells(window);
#endif //DEBUG_OCCUPIED_CELLS

    if (isDead()) {
        body.setFillColor(sf::Color(50, 50, 50));
        gun.setFillColor(sf::Color(50, 50, 50));
    }

    body.setPosition(viewModel->getPosition());
    window->draw(body);

    gun.setPosition(viewModel->getPosition());
    gun.setRotation(viewModel->getRotation());
    window->draw(gun);

    hpIndicator.setRadius(std::max(viewModel->getHealthPoints()* 0.3f, 0.f));
    hpIndicator.setOrigin(hpIndicator.getRadius(), hpIndicator.getRadius());
    hpIndicator.setPosition(viewModel->getPosition());
    window->draw(hpIndicator);

    nickname.setString(viewModel->getName());
    nickname.setOrigin(nickname.getLocalBounds().width/2, nickname.getLocalBounds().height);
    nickname.setPosition(viewModel->getPosition());
    window->draw(nickname);

#ifdef DEBUG_HITBOXES
    drawHitbox(window);
#endif //DEBUG_HITBOXES
}

void PlayerView::drawOccupiedCells(MockableRenderWindow *window) const {
    sf::RectangleShape cellShape(Cell::size);
    cellShape.setFillColor(sf::Color::Green);
    cellShape.setOutlineThickness(1);
    cellShape.setOutlineColor(sf::Color::Black);

    for (Cell *cell : viewModel->getOccupiedCells()) {
        cellShape.setPosition(cell->position);
        window->draw(cellShape);
    }
}

void PlayerView::drawHitbox(MockableRenderWindow *window) const {
    sf::RectangleShape hitbox;
    hitbox.setFillColor(sf::Color(100, 100, 100));
    hitbox.setSize(viewModel->getHitboxSize());
    hitbox.setPosition(viewModel->getHitboxTopLeft());
    window->draw(hitbox);
}

void PlayerView::update() {

    if (!isDead()) {
        bool left = keyboard->isKeyPressed(sf::Keyboard::A);
        bool right = keyboard->isKeyPressed(sf::Keyboard::D);
        bool up = keyboard->isKeyPressed(sf::Keyboard::W);
        bool down = keyboard->isKeyPressed(sf::Keyboard::S);

        if (left && up)
            viewModel->moveLeftUp();
        else if (left && down)
            viewModel->moveLeftDown();
        else if (right && up)
            viewModel->moveRightUp();
        else if (right && down)
            viewModel->moveRightDown();
        else if (left)
            viewModel->moveLeft();
        else if (right)
            viewModel->moveRight();
        else if (up)
            viewModel->moveUp();
        else if (down)
            viewModel->moveDown();
    }

    viewModel->rotate(calculateRotation());
}

void PlayerView::onSFMLEvent(sf::Event event) {

    if (isDead()) return;

    switch (event.type) {

        case sf::Event::MouseButtonPressed:
            sf::Transform rotation;
            rotation.rotate(calculateRotation(), viewModel->getPosition());
            sf::Vector2f vectorUp = viewModel->getPosition() - sf::Vector2f(0, body.getRadius() + gun.getSize().y );
            sf::Vector2f vectorGunOut = rotation.transformPoint(vectorUp);
            viewModel->shoot(vectorGunOut, normalize(calculateLookingDir()));
            break;
    }
}

float PlayerView::calculateRotation() {
    sf::Vector2f lookingDir = calculateLookingDir();
    float angle = angleBetween(sf::Vector2f(0, -1), lookingDir);
    return (lookingDir.x >= 0) ? angle : 360.f - angle;
}

sf::Vector2f PlayerView::calculateLookingDir() {
    return renderWindow->mapPixelToCoords(mouse->getMousePosition()) - viewModel->getPosition();
}


