//
// Created by Radoslaw Panuszewski on 11/16/18.
//

#ifndef ELKAROYALE_GAMEVIEW_H
#define ELKAROYALE_GAMEVIEW_H

class ScreenChangeListener;

#include <algorithm>
#include <set>
#include "views/screens/proxy/ScreenChangeListener.h"
#include "windows/MainWindow.h"
#include "ScreenType.h"
#include "render_windows/MockableRenderWindow.h"
#include "views/View.h"

/**
 * \brief Abstract class for managing low-level screen inputs and distributing it further
 */
class Screen: public View {

public:
    /// Registers listener for handling Screen's change requests
    /// \param listener - the listener to be registered
    void addOnScreenChangeListener(ScreenChangeListener *listener) {
        screenChangeListeners.push_back(listener);
    }

protected:
    /// Method called by sub-classes in order to request screen swap
    /// \param type - type of the screen we want to change to
    void requestScreenChange(ScreenType type) {
        for(auto listener : screenChangeListeners)
            listener->onScreenChangeRequest(type);
    }

private:
    std::vector<ScreenChangeListener*> screenChangeListeners {};
};

#endif //ELKAROYALE_GAMEVIEW_H
