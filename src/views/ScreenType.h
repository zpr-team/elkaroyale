//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifndef ELKAROYALE_SCREENTYPE_H
#define ELKAROYALE_SCREENTYPE_H


enum class ScreenType {
    MENU, GAME
};


#endif //ELKAROYALE_SCREENTYPE_H
