//
// Created by Radoslaw Panuszewski on 11/17/18.
//

#ifndef ELKAROYALE_ACTIVESCREENPROXY_H
#define ELKAROYALE_ACTIVESCREENPROXY_H

class Screen;

#include <fruit/fruit.h>
#include <map>
#include <memory>
#include "views/Screen.h"
#include "../menu/MenuScreen.h"
#include "../game/GameScreen.h"

struct Menu {};
struct Game {};

class CurrentScreenProxy: public Screen, public ScreenChangeListener {
public:
    INJECT(CurrentScreenProxy(
            ANNOTATED(Menu, Screen*) menuScreen, 
            ANNOTATED(Game, Screen*) gameScreen));

    void draw(MockableRenderWindow* window) override;

    void update() override;

    void onScreenChangeRequest(ScreenType type) override;

    void onSFMLEvent(sf::Event event) override;

private:
    ScreenType currentScreen;
    std::map<ScreenType, Screen*> screens;
};

fruit::Component
        <
        fruit::Required<MockableRenderWindow, SystemInfoProvider>,
        CurrentScreenProxy
        >
getScreenComponent();

#endif //ELKAROYALE_ACTIVESCREENPROXY_H
