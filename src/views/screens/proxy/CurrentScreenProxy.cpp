//
// Created by Radoslaw Panuszewski on 11/17/18.
//

#include <sys/socket.h>
#include <util/system_info/LinuxSystemInfoProvider.h>
#include "CurrentScreenProxy.h"

CurrentScreenProxy::CurrentScreenProxy(ANNOTATED(Menu, Screen*) menuScreen, ANNOTATED(Game, Screen*) gameScreen) {

    screens[ScreenType::MENU] = menuScreen;
    screens[ScreenType::GAME] = gameScreen;
    currentScreen = ScreenType::MENU;

    for (auto& pair: screens) {
        pair.second->addOnScreenChangeListener(this);
    }
}

void CurrentScreenProxy::onScreenChangeRequest(ScreenType screenType) {
    currentScreen = screenType;
}

void CurrentScreenProxy::draw(MockableRenderWindow* window) {
    screens[currentScreen]->draw(window);
}

void CurrentScreenProxy::update() {
    screens[currentScreen]->update();
}

void CurrentScreenProxy::onSFMLEvent(sf::Event event) {
    screens[currentScreen]->onSFMLEvent(event);
}

fruit::Component
        <
         fruit::Required<MockableRenderWindow, SystemInfoProvider>,
         CurrentScreenProxy
        >
getScreenComponent() {
    return fruit::createComponent()
            .bind<fruit::Annotated<Menu, Screen>, MenuScreen>()
            .bind<fruit::Annotated<Game, Screen>, GameScreen>();
}