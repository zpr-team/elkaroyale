//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifndef ELKAROYALE_SCREENCHANGELISTENER_H
#define ELKAROYALE_SCREENCHANGELISTENER_H


#include "views/ScreenType.h"

struct ScreenChangeListener {

    virtual void onScreenChangeRequest(ScreenType type) = 0;
};


#endif //ELKAROYALE_SCREENCHANGELISTENER_H
