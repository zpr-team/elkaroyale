//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#include "GameScreen.h"
#include <util/string_utils.h>

#ifdef DEBUG_GRID
GameScreen::GameScreen(
        GameScreenViewModel *viewModel,
        Keyboard *keyboard,
        Session *session,
        PlayerViewFactory playerViewFactory,
        BulletViewFactory bulletViewFactory,
        UniformGrid *uniformGrid) :
    viewModel(viewModel),
    keyboard(keyboard),
    session(session),
    playerViewFactory(playerViewFactory),
    bulletViewFactory(bulletViewFactory),
    uniformGrid(uniformGrid)
#else
GameScreen::GameScreen(
        GameScreenViewModel *viewModel,
        Keyboard *keyboard,
        Session *session,
        PlayerViewFactory playerViewFactory,
        BulletViewFactory bulletViewFactory) :
    viewModel(viewModel),
    keyboard(keyboard),
    session(session),
    playerViewFactory(playerViewFactory),
     bulletViewFactory(bulletViewFactory)
#endif //DEBUG_GRID
{
    viewModel->listenOnPlayerSpawned([this](PlayerViewModel *playerVM) {
        addPlayerView(playerVM);
    });

    viewModel->listenOnBulletSpawned([=](BulletViewModel *bulletVM) {
        addBulletView(bulletVM);
    });

    viewModel->listenOnPlayerDestroyed([=](sf::Int64 playerId) {
        playerViewsMutex.lock();
        playerViewsToDelete.push(playerId);
        playerViewsMutex.unlock();
    });

    viewModel->listenOnBulletDestroyed([=](sf::Int64 bulletId) {
        bulletViewsMutex.lock();
        bulletViews.erase(bulletId);
        bulletViewsMutex.unlock();
    });

    backgroundTexture.loadFromFile("bg.jpg");
    backgroundSprite.setTexture(backgroundTexture);
}

void GameScreen::addPlayerView(PlayerViewModel *playerVM) {

    long id = playerVM->getId();

    if (playerViews.count(id) != 0) return;

    std::unique_ptr<PlayerView> playerView = createPlayerView();
    playerView->setViewModel(playerVM);

    playerViewsMutex.lock();
    playerViews[id] = std::move(playerView);
    playerViewsMutex.unlock();
}

void GameScreen::addBulletView(BulletViewModel *bulletVM) {

    std::unique_ptr<BulletView> bulletView = createBulletView();
    bulletView->setViewModel(bulletVM);

    bulletViewsMutex.lock();
    bulletViews[bulletVM->getBulletId()] = std::move(bulletView);
    bulletViewsMutex.unlock();
}

void GameScreen::draw(MockableRenderWindow* window) {

    deleteDisconnectedPlayerViews();

    viewModel->deleteExpiringBullets();

    bulletViewsMutex.lock();
    for (auto& bulletViewPair : bulletViews)
        bulletViewPair.second->update();
    bulletViewsMutex.unlock();

    window->clear(sf::Color::White);

    window->draw(backgroundSprite);

#ifdef DEBUG_GRID
    drawGrid(window);
#endif // DEBUG_GRID

    playerViewsMutex.lock();
    for (auto& playerViewPair : playerViews)
        playerViewPair.second->draw(window);
    playerViewsMutex.unlock();

    bulletViewsMutex.lock();
    for (auto& bulletViewPair : bulletViews)
        bulletViewPair.second->draw(window);
    bulletViewsMutex.unlock();

    window->display();
}

void GameScreen::deleteDisconnectedPlayerViews() {
    playerViewsMutex.lock();
    while(!playerViewsToDelete.empty()) {
        sf::Int64 id = playerViewsToDelete.front();
        playerViewsToDelete.pop();
        playerViews.erase(id);
    }
    playerViewsMutex.unlock();
}

void GameScreen::drawGrid(MockableRenderWindow *window) const {
    sf::RectangleShape cellShape(Cell::size);
    cellShape.setFillColor(sf::Color::Transparent);
    cellShape.setOutlineThickness(1);
    cellShape.setOutlineColor(sf::Color::Black);

    for (auto row : uniformGrid->getCells()) {
        for (Cell &cell : row) {
            cellShape.setPosition(cell.position);
            window->draw(cellShape);
        }
    }
}

void GameScreen::update() {

    playerViewsMutex.lock();
    playerViews[session->getLocalPlayerId()]->update();
    playerViewsMutex.unlock();
}

void GameScreen::onSFMLEvent(sf::Event event) {

    if (event.type == sf::Event::Closed ||
        (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)) {
        viewModel->disconnect();
    }
    else {
        playerViewsMutex.lock();
        playerViews[session->getLocalPlayerId()]->onSFMLEvent(event);
        playerViewsMutex.unlock();
    }
}
