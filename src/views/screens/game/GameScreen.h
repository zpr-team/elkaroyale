//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifndef ELKAROYALE_GAMESCREEN_H
#define ELKAROYALE_GAMESCREEN_H

#include <views/Screen.h>
#include <views/player/PlayerView.h>
#include <util/Keyboard.h>
#include <view_models/game/GameScreenViewModel.h>
#include <views/bullet/BulletView.h>

/**
 * Screen which renders actual game, see \see GameScreenViewModel
 */
class GameScreen: public Screen {
public:
#ifdef DEBUG_GRID
    INJECT(GameScreen(
            GameScreenViewModel *viewModel,
            Keyboard *keyboard,
            Session *session,
            PlayerViewFactory playerViewFactory,
            BulletViewFactory bulletViewFactory,
            UniformGrid *uniformGrid));
#else
    INJECT(GameScreen(
            GameScreenViewModel *viewModel,
            Keyboard *keyboard,
            Session *session,
            PlayerViewFactory playerViewFactory,
            BulletViewFactory bulletViewFactory));
#endif //DEBUG_GRID
    virtual ~GameScreen() = default;
    virtual void draw(MockableRenderWindow*  window) override;
    virtual void update() override;

    void onSFMLEvent(sf::Event event) override;

private:
    void addPlayerView(PlayerViewModel *playerVM);
    void addBulletView(BulletViewModel *bulletVM);
    std::unique_ptr<PlayerView> createPlayerView() { return playerViewFactory(); }
    std::unique_ptr<BulletView> createBulletView() { return bulletViewFactory(); }
    void deleteDisconnectedPlayerViews();

    std::unordered_map<sf::Int64, std::unique_ptr<PlayerView>> playerViews;
    std::unordered_map<sf::Int64, std::unique_ptr<BulletView>> bulletViews;

    std::queue<sf::Int64> playerViewsToDelete;
    std::mutex playerViewsMutex;
    std::mutex bulletViewsMutex;

    GameScreenViewModel *viewModel;
    Keyboard *keyboard;
    Session *session;
    PlayerViewFactory playerViewFactory;
    BulletViewFactory bulletViewFactory;
    UniformGrid *uniformGrid;

    sf::Texture backgroundTexture;
    sf::Sprite backgroundSprite;

    void drawGrid(MockableRenderWindow *window) const;

};

#endif //ELKAROYALE_GAMESCREEN_H