//
// Created by Radoslaw Panuszewski on 11/17/18.
//

#include "MenuScreen.h"

MenuScreen::MenuScreen(MenuScreenViewModel *viewModel, MockableRenderWindow *window) : viewModel(viewModel), window(window) {

    boxToggle = false;
    font.loadFromFile("6809_chargen.ttf");
    nickInputString = "nickname";
    ipInputString = sf::IpAddress::getLocalAddress().toString();
    initTitle();
    initInputText();
}

void MenuScreen::draw(MockableRenderWindow* window) {

    window->clear(sf::Color::Blue);
    window->draw(title);

    inputTextUpdate();
    window->draw(nickInputBox);
    window->draw(ipInputBox);
    window->draw(nickInputText);
    window->draw(ipInputText);
    window->display();
}

void MenuScreen::update() {

}

void MenuScreen::onSFMLEvent(sf::Event event) {

    switch (event.type) {

        case sf::Event::KeyPressed:
            switch (event.key.code) {
                case sf::Keyboard::Return:
                    if (viewModel->connect(ipInputString) != -1 && viewModel->joinPlayer(nickInputString) != -1)
                        requestScreenChange(ScreenType::GAME);
                    break;
                case sf::Keyboard::Escape:
                    viewModel->disconnect();
                    break;
                case sf::Keyboard::Tab:
                    boxToggle = !boxToggle;
                default:
                    break;
            }
            break;

        case sf::Event::Closed:
            viewModel->disconnect();
            break;

        case sf::Event::TextEntered:
            switch (event.text.unicode) {
                case '\b':
                    if (!nickInputString.empty() && !boxToggle) { nickInputString.pop_back(); }
                    else if (!ipInputString.empty() && boxToggle) { ipInputString.pop_back(); }
                    break;
                default:
                    if (event.text.unicode != '\n' && event.text.unicode != '\r' && event.text.unicode != '\t') {
                        if (nickInputString.size()  < 20 && !boxToggle) nickInputString += event.text.unicode;
                        else if (ipInputString.size() < 15 && boxToggle) ipInputString += event.text.unicode;
                    }
                    break;
            }
            break;

        default:
            break;
    }
}

void MenuScreen::initTitle() {
    title = sf::Text(Constants::WINDOW_TITLE, font);
    title.setCharacterSize(70);
    title.setStyle(sf::Text::Bold);
    title.setFillColor(sf::Color::Cyan);
    title.setOutlineThickness(2.f);
    title.setOutlineColor(sf::Color::Black);

    sf::FloatRect rect = title.getLocalBounds();
    title.setOrigin(rect.left + rect.width / 2, rect.top + rect.height / 2);
    title.setPosition(window->getSize().x / 2, title.getPosition().y + 1.5f * rect.height);
}

void MenuScreen::initInputText() {
    nickInputText = sf::Text();
    nickInputText.setFont(font);
    nickInputText.setCharacterSize(35);
    nickInputText.setStyle(sf::Text::Regular);
    nickInputText.setFillColor(sf::Color::Blue);
    nickInputText.setOutlineThickness(1.f);
    nickInputText.setOutlineColor(sf::Color::Black);

    nickInputBox = sf::RectangleShape();
    nickInputBox.setFillColor(sf::Color::Cyan);
    nickInputBox.setOutlineColor(sf::Color::Black);
    nickInputBox.setOutlineThickness(2.f);

    nickInputText.setString("A");
    sf::FloatRect rect = nickInputText.getLocalBounds();
    nickInputBox.setSize(sf::Vector2f(500.f, 2.f * rect.height));
    nickInputBox.setOrigin(nickInputBox.getSize().x / 2.f, nickInputBox.getSize().y / 2.f);
    nickInputBox.setPosition(window->getSize().x / 2.f, window->getSize().y / 3.f );

    ipInputText = nickInputText;
    ipInputText.move(0.f, 4.f * rect.height);
    ipInputBox = nickInputBox;
    ipInputBox.move(0.f, 4.f * rect.height);
}

void MenuScreen::inputTextUpdate() {

    nickInputText.setString(nickInputString);
    sf::FloatRect rect = nickInputText.getLocalBounds();
    nickInputText.setOrigin(rect.left + rect.width / 2.f, rect.top + rect.height / 2.f);
    nickInputText.setPosition(window->getSize().x / 2.f, window->getSize().y / 3.f);

    ipInputText.setString(ipInputString);
    rect = ipInputText.getLocalBounds();
    ipInputText.setOrigin(rect.left + rect.width / 2.f, rect.top + rect.height / 2.f);
    ipInputText.setPosition(nickInputText.getPosition());
    ipInputText.move(0.0f, 4.f * rect.height);
}
