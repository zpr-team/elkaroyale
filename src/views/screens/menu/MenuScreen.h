//
// Created by Radoslaw Panuszewski on 11/17/18.
//

#ifndef ELKAROYALE_MENUSCREEN_H
#define ELKAROYALE_MENUSCREEN_H


#include <memory>
#include <fruit/macro.h>
#include "views/Screen.h"
#include <view_models/menu/MenuScreenViewModel.h>
#include <util/constants/Constants.h>

class MenuScreen: public Screen {

public:
    INJECT(MenuScreen(MenuScreenViewModel *viewModel, MockableRenderWindow *window));

    void draw(MockableRenderWindow*  window) override;

    void update() override;

    void onSFMLEvent(sf::Event event) override;

private:
    void initTitle();
    void initInputText();
    void inputTextUpdate();

    MockableRenderWindow *window;

    MenuScreenViewModel *viewModel;
    sf::Font font;
    sf::Text title;
    sf::Text nickLabel;
    sf::Text ipLabel;

    sf::Text nickInputText;
    sf::RectangleShape nickInputBox;
    std::string nickInputString;

    sf::Text ipInputText;
    sf::RectangleShape ipInputBox;
    std::string ipInputString;

    bool boxToggle;
};


#endif //ELKAROYALE_MENUSCREEN_H
