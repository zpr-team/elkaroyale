//
// Created by Radoslaw Panuszewski on 12/1/18.
//

#ifndef ELKAROYALE_DRAWABLE_H
#define ELKAROYALE_DRAWABLE_H

#include "render_windows/MockableRenderWindow.h"

/**
 * Interface for view, i.e. everything that can be drawn and has a corresponding ViewModel
 */
struct View {

    /// Render all contents and call draw() on each sub-view
    /// \param window
    virtual void draw(MockableRenderWindow* window) = 0;

    ///Called each frame, if the view has currently access to input. Mostly used for real-time user inputs
    virtual void update() = 0;

    ///Called when SFML event occures
    virtual void onSFMLEvent(sf::Event event) {}
};


#endif //ELKAROYALE_DRAWABLE_H
