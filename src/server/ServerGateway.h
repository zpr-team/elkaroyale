//
// Created by radek on 1/2/19.
//

#ifndef ELKAROYALE_SERVER_GATEWAY_H
#define ELKAROYALE_SERVER_GATEWAY_H

#include <functional>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <memory>
#include <thread>
#include <SFML/Network.hpp>
#include <models/ShotTimestamp.h>
#include <util/Session.h>
#include <Server.h>
#include <util/functional_typedefs.h>


class ServerGateway {
public:
    INJECT(ServerGateway(Session *session));
    ~ServerGateway();
    void listenOnStateUpdate(long id, GameStateConsumer listener);
    void listenOnPlayerJoined(PlayerTimestampConsumer listener);
    void listenOnBulletShot(ShotTimestampConsumer listener);
    void listenOnDisconnect(IdConsumer listener);
    void unregisterFromStateUpdate(long id);

    int requestJoinPlayer(const std::string &playerName);
    void requestMove(Direction direction);
    void requestSetRotation(float angle);
    void requestShoot(sf::Vector2f startPosition, sf::Vector2f direction);

    int connect(std::string &ipAddress);
    void disconnect();

private:
    void receiveGameState();
    void pingServer();
    RQ getMoveRequestCode(Direction direction);
    std::unique_ptr<GameStateTimestamp> gameStateTimestamp;

    std::unordered_map<long, GameStateConsumer> stateUpdateListeners;
    std::vector<PlayerTimestampConsumer> spawnListeners;
    std::vector<ShotTimestampConsumer> shootListeners;
    std::vector<IdConsumer> disconnectListeners;

    std::unique_ptr<std::thread> receiveThread;
    std::unique_ptr<std::thread> pingThread;
    sf::TcpSocket socket;
    const sf::IpAddress IP = sf::IpAddress::getLocalAddress();
    const unsigned short PORT = 2048;
    const sf::Time PING_INTERVAL = sf::seconds(1);
    Session *session;

    bool connected;
};

#endif //ELKAROYALE_SERVER_GATEWAY_H
