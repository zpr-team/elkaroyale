//
// Created by radek on 1/2/19.
//

#include <util/maths.h>
#include "ServerGateway.h"

sf::Socket::Status printStatus(sf::Socket::Status status);

ServerGateway::ServerGateway(Session *session) : session(session), receiveThread() {

    gameStateTimestamp = std::make_unique<GameStateTimestamp>();
}

ServerGateway::~ServerGateway() {
    connected = false;
    if (receiveThread)
        receiveThread->join();
    if (pingThread)
        pingThread->join();
}

void ServerGateway::listenOnStateUpdate(long id, GameStateConsumer listener) {
    stateUpdateListeners[id] = listener;
}

void ServerGateway::listenOnPlayerJoined(PlayerTimestampConsumer listener) {
    spawnListeners.push_back(listener);
}

void ServerGateway::listenOnBulletShot(ShotTimestampConsumer listener) {
    shootListeners.push_back(listener);
}

void ServerGateway::listenOnDisconnect(IdConsumer listener) {
    disconnectListeners.push_back(listener);
}

void ServerGateway::unregisterFromStateUpdate(long id) {
    stateUpdateListeners.erase(id);
}

int ServerGateway::requestJoinPlayer(const std::string &playerName) {
    if (!connected)
        return -1;

    sf::Packet packet;
    packet << RQ::SPAWN << playerName;
    socket.send(packet);

    PlayerTimestamp playerTimestamp;
    packet.clear();
    if (socket.receive(packet) == sf::Socket::Done) {
        packet >> playerTimestamp;
        session->setInitialServerTime(playerTimestamp.timeMillis);
        gameStateTimestamp->players[playerTimestamp.id] = playerTimestamp;

        session->setLocalPlayerName(playerTimestamp.name);

        for (auto& listener : spawnListeners) {
            listener(gameStateTimestamp->players[playerTimestamp.id]);
        }

        receiveThread = std::make_unique<std::thread>(&ServerGateway::receiveGameState, this);

        std::cout << "Joined(id: " << playerTimestamp.id << ")" << std::endl;
        return 0;
    }
    std::cout << "ERROR: Couldn't join" << std::endl;
    return -1;
}

int ServerGateway::connect(std::string &ipAddress) {
    if (connected)
        return 0;

    sf::Socket::Status status = socket.connect(sf::IpAddress(ipAddress), PORT, sf::seconds(4));
    sf::Packet packet;
    sf::Int64 id;

    std::cout << "Connection status: ";
    if (printStatus(status) == sf::Socket::Done) {
        pingThread = std::make_unique<std::thread>(&ServerGateway::pingServer, this);
        connected = true;
    } else {
        std::cout << "ERROR: Could not connect to server\n";
        return -1;
    }

    std::cout << "Id receiving status: ";
    if (printStatus(socket.receive(packet)) == sf::Socket::Done) {
        packet >> id;
        session->setLocalPlayerId(id);
    } else {
        std::cout << "ERROR: Could not retrieve id from server\n";
        return -1;
    }
    return 0;
}

void ServerGateway::disconnect() {
    sf::Packet packet;

    packet << RQ::DISCONNECT;
    socket.send(packet);
    socket.disconnect();

    std::cout << "Receive thread finished" << std::endl;
}

void ServerGateway::requestMove(Direction direction) {

    sf::Packet packet;
    packet << getMoveRequestCode(direction);
    socket.send(packet);
}

void ServerGateway::requestSetRotation(float angle) {

    sf::Packet packet;
    packet << RQ::SET_ROTATION << angle;
    socket.send(packet);
}

void ServerGateway::requestShoot(sf::Vector2f startPosition, sf::Vector2f direction) {

    sf::Packet packet;
    packet << RQ::SHOOT << startPosition.x << startPosition.y << direction.x << direction.y;
    socket.send(packet);
}

void ServerGateway::receiveGameState()
{
    PlayerTimestamp playerTimestamp;
    ShotTimestamp shotTimestamp;
    sf::Packet packet;
    sf::Int32 requestType;
    sf::Int64 id;
    while (socket.getRemoteAddress() != sf::IpAddress::None) {

        std::unordered_map<sf::Int64, PlayerTimestamp> &players = gameStateTimestamp->players;
        std::vector<HitTimestamp> &hits = gameStateTimestamp->unprocessedHits;

        packet.clear();
        socket.receive(packet);
        packet >> requestType;

        switch (requestType) {
            case RQ::UPDATE:
                sf::Uint64 playerCount;
                packet >> playerCount;
                for (int i = 0; i < playerCount; ++i) {
                    packet >> playerTimestamp;
                    if (players.count(playerTimestamp.id) == 0)
                        for (auto& listener: spawnListeners)
                            listener(playerTimestamp);
                    players[playerTimestamp.id] = playerTimestamp;
                }
                break;

            case RQ::DISCONNECT:
                packet >> id;
                players.erase(id);
                for (auto& listener : disconnectListeners) {
                    listener(id);
                }
                break;

            case RQ::SHOOT:
                packet >> shotTimestamp;
                for (auto& listener : shootListeners) {
                    listener(shotTimestamp);
                }

            default:
                break;
        }

        for (auto& listener: stateUpdateListeners)
            listener.second(gameStateTimestamp.get());
    }
    std::cout << "Receive thread exits loop" << std::endl;
}

void ServerGateway::pingServer()
{
    sf::Packet packet;
    while (socket.getRemoteAddress() != sf::IpAddress::None) {
        packet << RQ::PING;
        socket.send(packet);
        sf::sleep(PING_INTERVAL);
    }
}

RQ ServerGateway::getMoveRequestCode(Direction direction) {
    switch (direction) {
        case LEFT: return RQ::MOVE_LEFT;
        case RIGHT: return RQ::MOVE_RIGHT;
        case UP: return RQ::MOVE_UP;
        case DOWN: return RQ::MOVE_DOWN;
        case LEFT_UP: return RQ::MOVE_LEFT_UP;
        case LEFT_DOWN: return RQ::MOVE_LEFT_DOWN;
        case RIGHT_UP: return RQ::MOVE_RIGHT_UP;
        case RIGHT_DOWN: return RQ::MOVE_RIGHT_DOWN;
        default: throw std::runtime_error("unsupported request");
    }
}

sf::Socket::Status printStatus(sf::Socket::Status status)
{
    switch (status) {
        case sf::Socket::Done:
            std::cout << "Done" << std::endl;
            break;
        case sf::Socket::NotReady:
            std::cout << "NotReady" << std::endl;
            break;
        case sf::Socket::Partial:
            std::cout << "Partial" << std::endl;
            break;
        case sf::Socket::Disconnected:
            std::cout << "Disconnected" << std::endl;
            break;
        case sf::Socket::Error:
        default:
            std::cout << "Error" << std::endl;
    }
    return status;
}
