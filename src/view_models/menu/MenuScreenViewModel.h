//
// Created by radek on 1/3/19.
//

#ifndef ELKAROYALE_MENUSCREENVIEWMODEL_H
#define ELKAROYALE_MENUSCREENVIEWMODEL_H


#include <util/Session.h>
#include <server/ServerGateway.h>

class MenuScreenViewModel {
public:
    INJECT(MenuScreenViewModel(ServerGateway *server, Session *session));

    int joinPlayer(std::string name);
    virtual int connect(std::string ipAddress);
    virtual void disconnect();

private:
    ServerGateway *server;
    Session *session;
};


#endif //ELKAROYALE_MENUSCREENVIEWMODEL_H
