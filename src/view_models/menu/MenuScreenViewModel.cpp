//
// Created by radek on 1/3/19.
//

#include "MenuScreenViewModel.h"

MenuScreenViewModel::MenuScreenViewModel(ServerGateway *server, Session *session)
    : server(server),
      session(session) {}

int MenuScreenViewModel::joinPlayer(std::string name) {

    session->setLocalPlayerName(name);
    return server->requestJoinPlayer(name);
}

int MenuScreenViewModel::connect(std::string ipAddress) {
    return server->connect(ipAddress);
}

void MenuScreenViewModel::disconnect() {
    server->disconnect();
}
