//
// Created by radek on 1/10/19.
//

#include <util/maths.h>
#include "BulletViewModel.h"


BulletViewModel::BulletViewModel(UniformGrid *uniformGrid, sf::Int64 bulletId, ShotTimestamp timestamp) :
        bulletEntity(uniformGrid)
{
    bulletEntity.setBulletId(bulletId);
    bulletEntity.setPlayerId(timestamp.playerId);
    bulletEntity.setDirection(timestamp.direction);
    bulletEntity.setPosition(timestamp.position);
}

BulletViewModel::~BulletViewModel() {}

void BulletViewModel::continueFlight() {
    bulletEntity.continueFlight();
}

void BulletViewModel::listenOnBulletOutOfRange(IdConsumer listener) {
    bulletEntity.listenOnBulletOutOfRange(listener);
}

