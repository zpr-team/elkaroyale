//
// Created by radek on 1/10/19.
//

#ifndef ELKAROYALE_BULLETVIEWMODEL_H
#define ELKAROYALE_BULLETVIEWMODEL_H


#include <algorithm>
#include <memory>
#include <functional>
#include <models/ShotTimestamp.h>
#include <fruit/fruit.h>
#include <models/Entity.h>
#include <server/ServerGateway.h>

class BulletViewModel {

public:
    INJECT(BulletViewModel(
            UniformGrid * uniformGrid,
            ASSISTED(sf::Int64) bulletId,
            ASSISTED(ShotTimestamp) timestamp));

    ~BulletViewModel();

    sf::Int64 getBulletId() const { return bulletEntity.getBulletId(); }
    sf::Int64 getPlayerId() const { return bulletEntity.getPlayerId(); }
    sf::Vector2f getPosition() const { return bulletEntity.getPosition(); }
    sf::Vector2f getDirection() const { return bulletEntity.getDirection(); }

    void continueFlight();
    void listenOnBulletOutOfRange(IdConsumer listener);

private:
    BulletEntity bulletEntity;
};

using BulletVMFactory = std::function<std::unique_ptr<BulletViewModel>(sf::Int64, ShotTimestamp)>;


#endif //ELKAROYALE_BULLETVIEWMODEL_H
