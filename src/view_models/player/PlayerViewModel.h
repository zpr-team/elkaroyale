//
// Created by Radoslaw Panuszewski on 12/1/18.
//

#ifndef ELKAROYALE_PLAYERVIEWMODEL_H
#define ELKAROYALE_PLAYERVIEWMODEL_H


#include <fruit/macro.h>
#include <models/PlayerData.h>
#include <memory>
#include <server/ServerGateway.h>
#include <models/PlayerTimestamp.h>
#include <models/Entity.h>
#include <models/Cell.h>
#include <set>
#include <cmath>

using BulletEntityConsumer = std::function<void(BulletEntity*)>;

/**
 * ViewModel class for \see PlayerView
 */
class PlayerViewModel {

public:
    INJECT(PlayerViewModel(
            ServerGateway *server,
            UniformGrid *uniformGrid,
            ASSISTED(PlayerTimestamp) initialTimestamp));

    virtual ~PlayerViewModel();

    virtual void moveLeft();
    virtual void moveRight();
    virtual void moveUp();
    virtual void moveDown();
    virtual void moveLeftUp();
    virtual void moveLeftDown();
    virtual void moveRightUp();
    virtual void moveRightDown();
    virtual void rotate(float angle);
    virtual void shoot(sf::Vector2f position, sf::Vector2f direction);

    virtual void listenOnBulletShot(ShotTimestampConsumer listener);
    virtual void listenOnGotHit(IdConsumer listener);

    virtual sf::Int64 getId() const { return player.getId(); }
    virtual std::string getName() const { return player.getName(); }
    virtual short getHealthPoints() const { return player.getHealthPoints(); }
    virtual sf::Vector2f getPosition() const { return player.getPosition(); }
    virtual float getRotation() const { return player.getRotation(); }
    virtual sf::Color getColor() const { return player.getColor(); }
    virtual void setColor(sf::Color color) { player.setColor(color); }
    virtual std::set<Cell*> getOccupiedCells() { return player.getOccupiedCells(); }
    virtual sf::Vector2f getHitboxSize() const { return player.getHitboxSize(); }
    virtual sf::Vector2f getHitboxTopLeft() const { return getPosition() - getHitboxSize() / 2.f; }

private:
    static constexpr float PLAYER_STEP = 10.0f;
    static constexpr float PLAYER_DIAGONAL_STEP = static_cast<float>(sqrt(PLAYER_STEP * PLAYER_STEP / 2.f));

    PlayerEntity player;
    ServerGateway *server;
    std::vector<ShotTimestampConsumer> shootListeners;
};

using PlayerVMFactory = std::function<std::unique_ptr<PlayerViewModel>(PlayerTimestamp)>;

#endif //ELKAROYALE_PLAYERVIEWMODEL_H
