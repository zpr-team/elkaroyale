//
// Created by Radoslaw Panuszewski on 12/1/18.
//

#include <iostream>
#include "PlayerViewModel.h"

PlayerViewModel::PlayerViewModel(ServerGateway *server, UniformGrid *uniformGrid, PlayerTimestamp initialTimestamp) :
    server(server),
    player(uniformGrid)
{
    player.setId(initialTimestamp.id);
    player.setName(initialTimestamp.name);
    player.setHealthPoints(initialTimestamp.healthPoints);
    player.setPosition(initialTimestamp.position);
    player.setRotation(initialTimestamp.rotation);
    player.setColor(initialTimestamp.color);

    server->listenOnStateUpdate(getId(), [=](GameStateTimestamp *gameState) {

        PlayerTimestamp timestamp = gameState->players[getId()];

        if (getPosition() != timestamp.position) player.setPosition(timestamp.position);
        if (getRotation() != timestamp.rotation) player.setRotation(timestamp.rotation);
        player.setHealthPoints(timestamp.healthPoints);
        player.setColor(timestamp.color);

        player.checkCollisions();
    });
}

PlayerViewModel::~PlayerViewModel() {
    server->unregisterFromStateUpdate(getId());
}

/// EVENTS

void PlayerViewModel::listenOnBulletShot(ShotTimestampConsumer listener) {
    shootListeners.push_back(listener);
}

void PlayerViewModel::listenOnGotHit(IdConsumer listener) {
    player.listenOnGotHit(listener);
}

/// ACTIONS

void PlayerViewModel::moveLeft() {
    player.tryMoveLeft([=] {
        server->requestMove(Direction::LEFT);
    });
}

void PlayerViewModel::moveRight() {
    player.tryMoveRight([=] {
        server->requestMove(Direction::RIGHT);
    });
}

void PlayerViewModel::moveUp() {
    player.tryMoveUp([=] {
        server->requestMove(Direction::UP);
    });
}

void PlayerViewModel::moveDown() {
    player.tryMoveDown([=] {
        server->requestMove(Direction::DOWN);
    });
}

void PlayerViewModel::moveLeftUp() {
    player.tryMoveLeftUp([=](Direction actualDirection) {
        server->requestMove(actualDirection);
    });
}

void PlayerViewModel::moveLeftDown() {
    player.tryMoveLeftDown([=](Direction actualDirection) {
        server->requestMove(actualDirection);
    });
}

void PlayerViewModel::moveRightUp() {
    player.tryMoveRightUp([=](Direction actualDirection) {
        server->requestMove(actualDirection);
    });
}

void PlayerViewModel::moveRightDown() {
    player.tryMoveRightDown([=](Direction actualDirection) {
        server->requestMove(actualDirection);
    });
}

void PlayerViewModel::rotate(float angle) {
    player.setRotation(angle);
    server->requestSetRotation(angle);
}

void PlayerViewModel::shoot(sf::Vector2f position, sf::Vector2f direction) {
    server->requestShoot(position, direction);

    ShotTimestamp timestamp;
    timestamp.playerId = getId();
    timestamp.position = position;
    timestamp.direction = direction;

    for (auto& listener : shootListeners) {
        listener(timestamp);
    }
}