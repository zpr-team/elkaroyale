//
// Created by radek on 1/2/19.
//

#ifndef ELKAROYALE_GAMESCREENVIEWMODEL_H
#define ELKAROYALE_GAMESCREENVIEWMODEL_H


#include <server/ServerGateway.h>
#include <util/Session.h>
#include <view_models/player/PlayerViewModel.h>
#include <view_models/bullet/BulletViewModel.h>
#include <map>
#include <memory>
#include <views/player/PlayerView.h>
#include <util/Mouse.h>
#include <queue>

using PlayerVMConsumer = std::function<void(PlayerViewModel*)>;
using BulletVMConsumer = std::function<void(BulletViewModel*)>;

class GameScreenViewModel {
public:
    INJECT(GameScreenViewModel(
            ServerGateway *server,
            Session *session,
            PlayerVMFactory playerVMFactory,
            BulletVMFactory bulletVMFactory
           ));

    ~GameScreenViewModel();

    virtual void deleteExpiringBullets();
    virtual void listenOnPlayerSpawned(PlayerVMConsumer listener);
    virtual void listenOnBulletSpawned(BulletVMConsumer listener);
    virtual void listenOnPlayerDestroyed(IdConsumer listener);
    virtual void listenOnBulletDestroyed(IdConsumer listener);
    virtual void disconnect();

private:
    void spawnPlayer(PlayerTimestamp timestamp);
    void spawnBullet(ShotTimestamp timestamp);

    std::unique_ptr<PlayerViewModel> createPlayerVM(PlayerTimestamp timestamp) { return playerVMFactory(timestamp); }
    std::unique_ptr<BulletViewModel> createBulletVM(sf::Int64 bulletId, ShotTimestamp timestamp) { return bulletVMFactory(bulletId, timestamp); }

    std::unordered_map<sf::Int64, std::unique_ptr<PlayerViewModel>> playerVMs;
    std::unordered_map<sf::Int64, std::unique_ptr<BulletViewModel>> bulletVMs;
    std::queue<sf::Int64> expiringBullets;

    std::mutex playerVMsMutex;
    std::mutex bulletVMsMutex;
    std::mutex expiringBulletsMutex;

    ServerGateway *server;
    Session *session;
    PlayerVMFactory playerVMFactory;
    BulletVMFactory bulletVMFactory;

    std::vector<PlayerVMConsumer> onPlayerSpawnListeners;
    std::vector<BulletVMConsumer> onBulletSpawnListeners;
    std::vector<IdConsumer> onPlayerDestroyedListeners;
    std::vector<IdConsumer> onBulletDestroyedListeners;
};


#endif //ELKAROYALE_GAMESCREENVIEWMODEL_H
