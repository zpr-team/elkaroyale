//
// Created by radek on 1/2/19.
//

#include <view_models/bullet/BulletViewModel.h>
#include <iostream>
#include "GameScreenViewModel.h"
#include <util/maths.h>

using namespace std::placeholders;

GameScreenViewModel::GameScreenViewModel(
        ServerGateway *server,
        Session *session,
        PlayerVMFactory playerVMFactory,
        BulletVMFactory bulletVMFactory) :
    server(server),
    session(session),
    playerVMFactory(playerVMFactory),
    bulletVMFactory(bulletVMFactory),
    expiringBulletsMutex {}
{
    server->listenOnPlayerJoined([=](PlayerTimestamp timestamp) {
        spawnPlayer(timestamp);
    });

    server->listenOnBulletShot([=](ShotTimestamp timestamp) {
        if (timestamp.playerId != session->getLocalPlayerId())
            spawnBullet(timestamp);
    });

    server->listenOnDisconnect([=](sf::Int64 playerId) {
        if (playerId == session->getLocalPlayerId()) return;

        for (auto& listener : onPlayerDestroyedListeners) {
            listener(playerId);
        }
        playerVMsMutex.lock();
        playerVMs.erase(playerId);
        playerVMsMutex.unlock();
    });
}

GameScreenViewModel::~GameScreenViewModel() {
}

void GameScreenViewModel::spawnPlayer(PlayerTimestamp timestamp) {

    std::unique_ptr<PlayerViewModel> playerVM = createPlayerVM(timestamp);

    if(playerVM->getId() == session->getLocalPlayerId()) {
        playerVM->listenOnBulletShot([=](ShotTimestamp timestamp) {
            spawnBullet(timestamp);
        });
    }

    for (auto& listener : onPlayerSpawnListeners) {
        listener(playerVM.get());
    }

    playerVMsMutex.lock();
    playerVMs[playerVM->getId()] = std::move(playerVM);
    playerVMsMutex.unlock();
}

void GameScreenViewModel::spawnBullet(ShotTimestamp timestamp) {

    static sf::Int64 nextBulletid = 0;

    std::unique_ptr<BulletViewModel> bulletVM = createBulletVM(nextBulletid++, timestamp);

    bulletVM->listenOnBulletOutOfRange([=](sf::Int64 bulletId) {
        expiringBulletsMutex.lock();
        expiringBullets.push(bulletId);
        expiringBulletsMutex.unlock();
    });

    for (auto& listener : onBulletSpawnListeners) {
        listener(bulletVM.get());
    }

    bulletVMsMutex.lock();
    bulletVMs[bulletVM->getBulletId()] = std::move(bulletVM);
    bulletVMsMutex.unlock();
}

/// EVENTS

void GameScreenViewModel::listenOnPlayerSpawned(PlayerVMConsumer listener) {
    onPlayerSpawnListeners.push_back(listener);
}

void GameScreenViewModel::listenOnBulletSpawned(BulletVMConsumer listener) {
    onBulletSpawnListeners.push_back(listener);
}

void GameScreenViewModel::listenOnPlayerDestroyed(IdConsumer listener) {
    onPlayerDestroyedListeners.push_back(listener);
}

void GameScreenViewModel::listenOnBulletDestroyed(IdConsumer listener) {
    onBulletDestroyedListeners.push_back(listener);
}

/// ACTIONS

void GameScreenViewModel::disconnect() {
    server->disconnect();
}

void GameScreenViewModel::deleteExpiringBullets() {

    while (true) {
        expiringBulletsMutex.lock();
        if (expiringBullets.empty()) {
            expiringBulletsMutex.unlock();
            break;
        }
        sf::Int64 bulletId = expiringBullets.front();
        expiringBullets.pop();
        expiringBulletsMutex.unlock();

        for (auto& listener : onBulletDestroyedListeners) {
            listener(bulletId);
        }
        bulletVMsMutex.lock();
        bulletVMs.erase(bulletId);
        bulletVMsMutex.unlock();
    }
}