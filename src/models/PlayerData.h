//
// Created by radek on 1/2/19.
//

#ifndef ELKAROYALE_PLAYER_H
#define ELKAROYALE_PLAYER_H


#include <string>
#include <fruit/macro.h>
#include <SFML/Config.hpp>

struct PlayerData {

    sf::Int64 id;
    std::string name;

public:
    PlayerData() = default;
    PlayerData(long id, std::string name): id(id), name(name) {}

};


#endif //ELKAROYALE_PLAYER_H
