//
// Created by Radoslaw Panuszewski on 1/16/19.
//

#ifndef ELKAROYALE_HITTIMESTAMP_H
#define ELKAROYALE_HITTIMESTAMP_H


#include <SFML/Main.hpp>

struct HitTimestamp {

    sf::Int64 shooterId;
    sf::Int64 victimId;
    sf::Int32 timeMillis;
};


#endif //ELKAROYALE_HITTIMESTAMP_H
