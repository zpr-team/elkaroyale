//
// Created by radek on 1/2/19.
//

#ifndef ELKAROYALE_GAMESTATE_H
#define ELKAROYALE_GAMESTATE_H


#include <unordered_map>
#include <PlayerEntity.h>
#include <BulletEntity.h>
#include <queue>
#include <iostream>
#include "HitTimestamp.h"

class GameState {
public:
    void startTime() { clock.restart(); }
    sf::Time getCurrentTime() { return clock.getElapsedTime(); }
    PlayerEntity *getPlayer(sf::Int64 id) { return players[id].get(); }
    BulletEntity *getBullet(sf::Int64 id) { return bullets[id].get(); }
    void addPlayer(std::unique_ptr<PlayerEntity> playerEntity);
    void addBullet(std::unique_ptr<BulletEntity> bullet);
    void deletePlayer(sf::Int64 id) { players.erase(id); }
    void deleteBullet(sf::Int64 id) { bullets.erase(id); }
    const std::unordered_map<sf::Int64, std::unique_ptr<PlayerEntity>> &getPlayers() const { return players; };
    const std::unordered_map<sf::Int64, std::unique_ptr<BulletEntity>> &getBullets() { return bullets; }
    bool hasUnprocessedHits() { return unprocessedHits.empty(); } 
    void deleteExpiringBullets();

private:
    static const int HIT_DMG = 5;
    std::unordered_map<sf::Int64, std::unique_ptr<PlayerEntity>> players;
    std::unordered_map<sf::Int64, std::unique_ptr<BulletEntity>> bullets;
    std::queue<sf::Int64> expiringBullets;
    std::queue<HitTimestamp> unprocessedHits;
    sf::Clock clock;
};


#endif //ELKAROYALE_GAMESTATE_H
