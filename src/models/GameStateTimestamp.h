//
// Created by Radoslaw Panuszewski on 1/13/19.
//

#ifndef ELKAROYALE_GAMESTATETIMESTAMP_H
#define ELKAROYALE_GAMESTATETIMESTAMP_H

#include <unordered_map>
#include <models/PlayerTimestamp.h>
#include <queue>
#include "HitTimestamp.h"

struct GameStateTimestamp {

    std::unordered_map<sf::Int64, PlayerTimestamp> players;
//    std::unordered_map<sf::Int64, ShotTimestamp>
    std::vector<HitTimestamp> unprocessedHits;
    sf::Int32 timeMillis;
};


#endif //ELKAROYALE_GAMESTATETIMESTAMP_H
