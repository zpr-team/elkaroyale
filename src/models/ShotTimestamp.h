//
// Created by Radoslaw Panuszewski on 1/10/19.
//

#ifndef ELKAROYALE_SHOTTIMESTAMP_H
#define ELKAROYALE_SHOTTIMESTAMP_H


#include <SFML/Config.hpp>
#include <SFML/System.hpp>

struct ShotTimestamp {

    sf::Int64 playerId;
    sf::Vector2f position;
    sf::Vector2f direction;
    sf::Int64 timeMillis;
};


#endif //ELKAROYALE_SHOTTIMESTAMP_H
