//
// Created by Radoslaw Panuszewski on 1/16/19.
//

#include "GameState.h"

void GameState::addPlayer(std::unique_ptr<PlayerEntity> playerEntity) {

    sf::Int64 playerId = playerEntity->getId();

    playerEntity->listenOnGotHit([=](sf::Int64 shooterId) {
        static int i = 0;
        std::cout << "Collision! " << i++ << std::endl;
        getPlayer(playerId)->applyDamge(HIT_DMG);
    });
    players[playerEntity->getId()] = std::move(playerEntity);
}

void GameState::addBullet(std::unique_ptr<BulletEntity> bullet) {
    bullet->listenOnBulletOutOfRange([=](sf::Int64 bulletId) {
        expiringBullets.push(bulletId);
    });
    bullets[bullet->getBulletId()] = std::move(bullet);
}

void GameState::deleteExpiringBullets() {
    while (!expiringBullets.empty()) {
        sf::Int64 bulletId = expiringBullets.front();
        expiringBullets.pop();
        deleteBullet(bulletId);
    }
}
