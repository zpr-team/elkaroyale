//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#ifndef ELKAROYALE_UNIFORMMAP_H
#define ELKAROYALE_UNIFORMMAP_H

#include <cstdlib>
#include <array>
#include <fruit/fruit.h>
#include "Cell.h"
#include <util/maths.h>
#include <set>
#include <SFML/System.hpp>

template <class T, size_t ROW, size_t COL>
using Table = std::array<std::array<T, COL>, ROW>;

class UniformGrid {

private:
    static constexpr int WIDTH = 16;
    static constexpr int HEIGHT = 16;

public:
    INJECT(UniformGrid());

    std::set<Cell*> findOccupiedCells(Entity *entity);

    Table<Cell, WIDTH, HEIGHT>& getCells() { return cells; }

    sf::Vector2f getMapSize() { return sf::Vector2f(WIDTH * Cell::size.x, HEIGHT * Cell::size.y); }
    
    void lock() { mutex.lock(); }
    void unlock() { mutex.unlock(); }

private:
    Table<Cell, WIDTH, HEIGHT> cells;
    std::mutex mutex;
};


#endif //ELKAROYALE_UNIFORMMAP_H
