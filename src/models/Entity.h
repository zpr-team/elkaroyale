//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#ifndef ELKAROYALE_ENTITY_H
#define ELKAROYALE_ENTITY_H


#include <SFML/System.hpp>
#include <list>
#include "Cell.h"
#include "UniformGrid.h"
#include <set>
#include <util/maths.h>
#include <SFML/Graphics/Color.hpp>

class Entity;
enum Direction { LEFT, RIGHT, UP, DOWN, LEFT_UP, LEFT_DOWN, RIGHT_UP, RIGHT_DOWN };
using EntityConsumer = std::function<void(Entity*)>;

class Entity {

public:
    Entity(sf::Vector2f hitbox, UniformGrid *uniformGrid);
    ~Entity();

    virtual sf::Vector2f getPosition() const { return position; }
    virtual float getX() const { return position.x; }
    virtual float getY() const { return position.y; }
    virtual void setPosition(const sf::Vector2f &position) { Entity::position = position; checkCollisions(); }
    virtual void setX(float x) { this->position.x = x; }
    virtual void setY(float y) { this->position.y = y; }

    virtual sf::Vector2f getHitboxSize() const { return hitbox; }
    virtual void setHitboxSize(const sf::Vector2f &hitbox) { Entity::hitbox = hitbox; }
    virtual sf::Vector2f getHitboxTopLeft() const { return getPosition() - getHitboxSize() / 2.f; }

    virtual std::set<Cell*> getOccupiedCells() { return occupiedCells; }
    virtual const sf::Color &getColor() const { return color; }
    virtual void setColor(sf::Color color) { this->color = color; }

    virtual bool collides(const Entity *other);
    virtual bool checkCollisions();
    virtual void listenOnCollision(EntityConsumer listener);

protected:
    virtual void onCollision(Entity *collidingEntity);
    sf::Vector2f position;
    sf::Vector2f hitbox;
    sf::Color color;
    std::set<Cell*> occupiedCells;
    UniformGrid *uniformGrid;

private:
    std::vector<EntityConsumer> collisionListeners;
};

bool operator<(const Entity &left, const Entity &right);


#endif //ELKAROYALE_ENTITY_H
