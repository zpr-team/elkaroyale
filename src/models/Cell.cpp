//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#include "Cell.h"

const sf::Vector2f Cell::size = sf::Vector2f(256.f, 256.f);

bool operator<(const Cell &left, const Cell &right) {
    return left.position.x < right.position.x;
}

std::ostream& operator<<(std::ostream &out, const Cell &cell) {
    out << "(" << cell.row << ", " << cell.col << ")";
    return out;
}