//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#include "UniformGrid.h"
#include "Entity.h"

UniformGrid::UniformGrid() {

    for (int i = 0; i < WIDTH; ++i) {
        for (int j = 0; j < HEIGHT; ++j) {
            cells[i][j].position = sf::Vector2f(i * Cell::size.x, j * Cell::size.y);
            cells[i][j].row = i;
            cells[i][j].col = j;
        }
    }
}

std::set<Cell*> UniformGrid::findOccupiedCells(Entity *entity) {

//    std::scoped_lock(mutex);
    std::set<Cell*> occupiedCells;

    for (auto& row : cells) {
        for (auto& cell : row) {
            if (entity->getHitboxTopLeft().x > cell.position.x + cell.size.x ||
                entity->getHitboxTopLeft().x + entity->getHitboxSize().x < cell.position.x ||
                entity->getHitboxTopLeft().y > cell.position.y + cell.size.y ||
                entity->getHitboxTopLeft().y + entity->getHitboxSize().y < cell.position.y)
            {
                cell.residentEntities.erase(entity);
            }
            else {
                occupiedCells.insert(&cell);
                cell.residentEntities.insert(entity);
            }
        }
    }
    return occupiedCells;
}
