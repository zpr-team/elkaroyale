//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#ifndef ELKAROYALE_CELL_H
#define ELKAROYALE_CELL_H


#include <vector>
#include <SFML/System.hpp>
#include <util/maths.h>
#include <set>
#include <util/maths.h>
class Entity;


struct Cell {
    std::set<Entity*> residentEntities;
    sf::Vector2f position;
    int row, col;

    static const sf::Vector2f size;
};

bool operator<(const Cell &left, const Cell &right);

std::ostream& operator<<(std::ostream &out, const Cell &cell);


#endif //ELKAROYALE_CELL_H
