//
// Created by Radoslaw Panuszewski on 1/11/19.
//

#include <iostream>
#include "Entity.h"

Entity::Entity(sf::Vector2f hitbox, UniformGrid *uniformGrid) :
        hitbox(hitbox),
        uniformGrid(uniformGrid)
{}

Entity::~Entity() {
    uniformGrid->lock();
    for (Cell *cell : occupiedCells) {
        cell->residentEntities.erase(this);
    }
    uniformGrid->unlock();
}

bool operator<(const Entity &left, const Entity &right) {
    return left.getPosition().x < right.getPosition().x;
}

bool Entity::collides(const Entity *other) {

    if (other == this) return false;
    if (other == nullptr) return false;

    return !(this->getHitboxTopLeft().x >= other->getHitboxTopLeft().x + other->getHitboxSize().x ||
             this->getHitboxTopLeft().x + this->getHitboxSize().x <= other->getHitboxTopLeft().x ||
             this->getHitboxTopLeft().y >= other->getHitboxTopLeft().y + other->getHitboxSize().y ||
             this->getHitboxTopLeft().y + this->getHitboxSize().y <= other->getHitboxTopLeft().y);
}

bool Entity::checkCollisions() {
    bool collisionDetected = false;
    uniformGrid->lock();
    occupiedCells = uniformGrid->findOccupiedCells(this);
    for (Cell *cell: occupiedCells) {
        for (Entity *entity : cell->residentEntities) {
            if (collides(entity)) {
                entity->onCollision(this);
                onCollision(entity);
                collisionDetected = true;
            }
        }
    }
    uniformGrid->unlock();
    return collisionDetected;
}

void Entity::listenOnCollision(EntityConsumer listener) {
    collisionListeners.push_back(listener);
}

void Entity::onCollision(Entity *collidingEntity) {
    for (auto& listener : collisionListeners) {
        listener(collidingEntity);
    }
}