//
// Created by Radoslaw Panuszewski on 1/7/19.
//

#ifndef ELKAROYALE_PLAYERTIMESTAMP_H
#define ELKAROYALE_PLAYERTIMESTAMP_H


#include <SFML/System.hpp>
#include "PlayerData.h"

struct PlayerTimestamp {

    sf::Int64 id;
    std::string name;
    sf::Color color;
    sf::Vector2f position;
    float rotation;
    short healthPoints;
    sf::Int32 timeMillis;

    PlayerTimestamp() = default;
};


#endif //ELKAROYALE_PLAYERTIMESTAMP_H
