//
// Created by Radoslaw Panuszewski on 11/30/18.
//

/**
 * This module deals with 2 problems with sf::RenderWindow
 * 1. Its methods are not virtual and thus cannot be mocked for testing
 * 2. FakeIt framework does not support multiple inheritance (which is the case for sf::RenderWindow
 */

#ifndef ELKAROYALE_RENDERWINDOWADAPTER_H
#define ELKAROYALE_RENDERWINDOWADAPTER_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <memory>
#include <fruit/component.h>
#include <fruit/fruit.h>
#include "../util/system_info/SystemInfoProvider.h"

/**
 * Helper class made due to FakeIt not supporting mocking classes with multiple inheritance
 * NOTE: We can make Mock<RenderWindowMockInterface> but not Mock<MockableRenderWindow>, because the second one
 * inherits from sf::RenderWindow which has multiple base classes (and FakeIt does not support it)
 */
struct RenderWindowMockInterface {
    virtual bool pollEvent(sf::Event& event) = 0;
    virtual bool isOpen() const = 0;
    virtual void clear(const sf::Color& color) = 0;
    virtual void display() = 0;
    virtual void close() = 0;
    virtual bool hasFocus() = 0;
};

/**
 * Helper class which just hides some methods from sf::RenderWindow and makes them virtual
 * in order to make it possible override them for testing (i.e. to mock them)
 * NOTE: This class has nothing to do with FakeIt not supporting multiple inheritance,
 * it just fixes problem with sf::RenderWindow methods not being virtual
 */
class MockableRenderWindow: public sf::RenderWindow, public RenderWindowMockInterface {

public:
    MockableRenderWindow() = default;

    MockableRenderWindow(
            sf::VideoMode mode,
            const sf::String& title,
            sf::Uint32 style = sf::Style::Default,
            const sf::ContextSettings& settings = sf::ContextSettings())
        : sf::RenderWindow(mode, title, style, settings) {}

    MockableRenderWindow(sf::WindowHandle handle,
            const sf::ContextSettings &settings)
        : sf::RenderWindow(handle, settings) {}

    virtual bool pollEvent(sf::Event& event) override {
        return sf::RenderWindow::pollEvent(event);
    }

    virtual bool isOpen() const override {
        return sf::RenderWindow::isOpen();
    }

    virtual void clear(const sf::Color &color) override {
        sf::RenderWindow::clear(color);
    }

    virtual void display() override {
        sf::RenderWindow::display();
    }

    virtual void close() override {
        sf::RenderWindow::close();
    }

    bool hasFocus() override {
        return sf::RenderWindow::hasFocus();
    }
};

#endif //ELKAROYALE_RENDERWINDOWADAPTER_H
