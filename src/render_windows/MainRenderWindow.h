//
// Created by Radoslaw Panuszewski on 11/30/18.
//

#ifndef ELKAROYALE_MAINRENDERWINDOW_H
#define ELKAROYALE_MAINRENDERWINDOW_H


#include "MockableRenderWindow.h"
#include "../util/constants/Constants.h"

/**
 * Customized class, gets the title and proper bounds
 */
class MainRenderWindow: public MockableRenderWindow {

public:
    INJECT(MainRenderWindow(
            SystemInfoProvider *systemProvider))

    : MockableRenderWindow(
        sf::VideoMode(
                /*systemProvider->getScreenSize().width,
                systemProvider->getScreenSize().height),*/ 1000, 725),
        Constants::WINDOW_TITLE,
        sf::Style::Titlebar | sf::Style::Close) {}
};


#endif //ELKAROYALE_MAINRENDERWINDOW_H
