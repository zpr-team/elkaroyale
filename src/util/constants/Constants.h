//
// Created by Radoslaw Panuszewski on 12/5/18.
//

#ifndef ELKAROYALE_CONSTANTS_H
#define ELKAROYALE_CONSTANTS_H


#include <string>

struct Constants {

    static const std::string WINDOW_TITLE;

};


#endif //ELKAROYALE_CONSTANTS_H
