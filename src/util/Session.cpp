//
// Created by radek on 1/3/19.
//

#include "Session.h"

sf::Int64 Session::getLocalPlayerId() const {
    return localPlayerId;
}

void Session::setLocalPlayerId(sf::Int64 localPlayerId) {
    Session::localPlayerId = localPlayerId;
}

const std::string &Session::getLocalPlayerName() const {
    return localPlayerName;
}

void Session::setLocalPlayerName(const std::string &localPlayerName) {
    Session::localPlayerName = localPlayerName;
}
