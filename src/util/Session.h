//
// Created by radek on 1/3/19.
//

#ifndef ELKAROYALE_SESSION_H
#define ELKAROYALE_SESSION_H


#include <fruit/macro.h>
#include <SFML/Main.hpp>
#include <SFML/System.hpp>

class Session {
public:
    INJECT(Session()) = default;

    virtual sf::Int64 getLocalPlayerId() const;
    virtual void setLocalPlayerId(sf::Int64 localPlayerId);
    virtual const std::string &getLocalPlayerName() const;
    virtual void setLocalPlayerName(const std::string &localPlayerName);
    virtual void setInitialServerTime(sf::Int32 time) { initialServerTime = sf::milliseconds(time); }

private:
    sf::Int64 localPlayerId;
    std::string localPlayerName;
    sf::Time initialServerTime;
};


#endif //ELKAROYALE_SESSION_H
