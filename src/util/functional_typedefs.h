//
// Created by Radoslaw Panuszewski on 1/16/19.
//

#ifndef ELKAROYALE_FUNCTIONAL_TYPEDEFS_H
#define ELKAROYALE_FUNCTIONAL_TYPEDEFS_H

#include <models/PlayerTimestamp.h>
#include <models/ShotTimestamp.h>
#include <models/GameStateTimestamp.h>

using PlayerTimestampConsumer = std::function<void(PlayerTimestamp)>;
using ShotTimestampConsumer = std::function<void(ShotTimestamp)>;
using GameStateConsumer = std::function<void(GameStateTimestamp*)>;
using IdConsumer = std::function<void(sf::Int64)>;

#endif //ELKAROYALE_FUNCTIONAL_TYPEDEFS_H
