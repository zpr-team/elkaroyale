//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifndef ELKAROYALE_BOUNDS_H
#define ELKAROYALE_BOUNDS_H


struct Bounds {

    Bounds(unsigned int width, unsigned int height) : width(width), height(height) {}

    unsigned int width;
    unsigned int height;
};


#endif //ELKAROYALE_BOUNDS_H
