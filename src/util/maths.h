//
// Created by Radoslaw Panuszewski on 1/8/19.
//

#ifndef ELKAROYALE_MATHS_H
#define ELKAROYALE_MATHS_H

#include <cmath>
#include <ostream>
#include <SFML/System.hpp>
#include <set>

float radiansToDegrees(float radians);

float length(const sf::Vector2f& v);

float scalarProduct(const sf::Vector2f& a, const sf::Vector2f& b);

float angleBetween(const sf::Vector2f& a, const sf::Vector2f& b);

sf::Vector2f normalize(const sf::Vector2f &v);

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

std::ostream& operator<<(std::ostream& out, const sf::Vector2f& v);

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::set<T>& set) {
    out << "[ ";
    for (T elem : set) {
        out << elem << ", ";
    }
    out << " ]";
    return out;
}

template <typename T>
std::ostream& operator<<(std::ostream &out, std::set<T*> &set) {
    out << "[ ";
    for (T *ptr : set) {
        out << *ptr << " ";
    }
    out << " ]";
    return out;
}

#endif //ELKAROYALE_MATHS_H
