//
// Created by radek on 12/9/18.
//

#ifndef ELKAROYALE_KEYBOARD_H
#define ELKAROYALE_KEYBOARD_H


#include <fruit/fruit.h>
#include <SFML/Graphics.hpp>

class Keyboard {

public:
    INJECT(Keyboard()) = default;

    virtual bool isKeyPressed(sf::Keyboard::Key keyCode) {
        return sf::Keyboard::isKeyPressed(keyCode);
    }
};


#endif //ELKAROYALE_KEYBOARD_H
