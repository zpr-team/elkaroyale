//
// Created by Radoslaw Panuszewski on 11/30/18.
//

#ifdef _WIN32

#ifndef ELKAROYALE_WINDOWSSYSTEMINFOPROVIDER_H
#define ELKAROYALE_WINDOWSSYSTEMINFOPROVIDER_H


#include "SystemInfoProvider.h"
#include "../../Bounds.h"

class WindowsSystemInfoProvider: public SystemInfoProvider {

public:
    Bounds getScreenSize() override;
};


#endif //ELKAROYALE_WINDOWSSYSTEMINFOPROVIDER_H

#endif //_WIN32