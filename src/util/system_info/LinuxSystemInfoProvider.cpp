//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifdef __linux__

#include "LinuxSystemInfoProvider.h"
#include <X11/Xlib.h>

Bounds LinuxSystemInfoProvider::getScreenSize() {
    Display* display = XOpenDisplay(nullptr);
    Screen*  screen = DefaultScreenOfDisplay(display);
    return { static_cast<unsigned int>(screen->width),
             static_cast<unsigned int>(screen->height) };
}

#endif //__linux__