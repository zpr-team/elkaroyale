//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifndef ELKAROYALE_SYSTEMPROPERTIESPROVIDER_H
#define ELKAROYALE_SYSTEMPROPERTIESPROVIDER_H

#include "../Bounds.h"

struct SystemInfoProvider {

    virtual Bounds getScreenSize() = 0;
};


#endif //ELKAROYALE_SYSTEMPROPERTIESPROVIDER_H
