//
// Created by Radoslaw Panuszewski on 11/29/18.
//

#ifdef __linux__

#ifndef ELKAROYALE_LINUXSYSTEMPROPERTIESPROVIDER_H
#define ELKAROYALE_LINUXSYSTEMPROPERTIESPROVIDER_H


#include <fruit/fruit.h>
#include "SystemInfoProvider.h"

class LinuxSystemInfoProvider: public SystemInfoProvider {
public:
    INJECT(LinuxSystemInfoProvider()) = default;

    Bounds getScreenSize() override;
};


#endif //ELKAROYALE_LINUXSYSTEMPROPERTIESPROVIDER_H

#endif //__linux__