//
// Created by Radoslaw Panuszewski on 11/30/18.
//

#ifdef _WIN32

#include "wtypes.h"
#include "WindowsSystemInfoProvider.h"

Bounds WindowsSystemInfoProvider::getScreenSize() {
    RECT desktop;
    const HWND hDesktop = GetDesktopWindow();
    GetWindowRect(hDesktop, &desktop);
    return { static_cast<unsigned int>(desktop.right),
             static_cast<unsigned int>(desktop.bottom) };
}

#endif //_WIN32
