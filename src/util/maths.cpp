//
// Created by Radoslaw Panuszewski on 1/8/19.
//

#include "maths.h"

float radiansToDegrees(float radians) {
    return radians * (180.0 / M_PI);
}

float length(const sf::Vector2f& v) {
    return sqrt(v.x * v.x + v.y * v.y);
}

float scalarProduct(const sf::Vector2f& a, const sf::Vector2f& b) {

    return a.x * b.x + a.y * b.y;
}

float angleBetween(const sf::Vector2f& a, const sf::Vector2f& b) {
    if (length(a) * length(b) == 0) return 0;
    return radiansToDegrees(acos(scalarProduct(a, b) / (length(a) * length(b))));
}

sf::Vector2f normalize(const sf::Vector2f &v) {
    float len = length(v);
    return sf::Vector2f(v.x / len, v.y / len);
}

std::ostream& operator<<(std::ostream& out, const sf::Vector2f& v) {
    out << "(" << v.x << ", " << v.y << ")";
    return out;
}