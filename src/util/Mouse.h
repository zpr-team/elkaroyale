//
// Created by Radoslaw Panuszewski on 1/7/19.
//

#include <fruit/macro.h>
#include <SFML/Window/Mouse.hpp>
#include <windows/MainWindow.h>
#include <util/system_info/SystemInfoProvider.h>
#include <render_windows/MockableRenderWindow.h>

#ifndef ELKAROYALE_MOUSE_H
#define ELKAROYALE_MOUSE_H

class Mouse {

public:
    INJECT(Mouse(MockableRenderWindow *renderWindow)): renderWindow(renderWindow) {}

    virtual bool isButtonPressed(sf::Mouse::Button button) {
        return sf::Mouse::isButtonPressed(button);
    }

    virtual sf::Vector2i getMousePosition() {
        return sf::Mouse::getPosition(*renderWindow);
    }

private:
    MockableRenderWindow *renderWindow;
};

#endif //ELKAROYALE_MOUSE_H