#include <fruit/fruit.h>
#include <windows/MainWindow.h>

int main() {

    fruit::Injector<Window> injector(getMainWindowComponent);
    Window *window = injector.get<Window*>();
    window->show();

    return 0;
}