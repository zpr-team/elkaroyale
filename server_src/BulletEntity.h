//
// Created by Radoslaw Panuszewski on 1/15/19.
//

#ifndef ELKAROYALE_BULLETENTITY_H
#define ELKAROYALE_BULLETENTITY_H


#include <models/Entity.h>
#include <util/functional_typedefs.h>

class BulletEntity: public Entity {

public:
    BulletEntity(UniformGrid *uniformGrid);

    inline sf::Int64 getPlayerId() const { return playerId; }
    inline sf::Int64 getBulletId() const { return bulletId; }
    inline const sf::Vector2f &getDirection() const { return direction; }

    void setPlayerId(sf::Int64 playerId) { BulletEntity::playerId = playerId;}
    void setBulletId(sf::Int64 bulletId) { BulletEntity::bulletId = bulletId; }
    void setDirection(const sf::Vector2f &direction) { BulletEntity::direction = direction; }

    void continueFlight();
    void listenOnBulletOutOfRange(IdConsumer listener);

protected:
    void onCollision(Entity *collidingEntity) override;
    void onOutOfRange();

private:
    std::vector<IdConsumer> bulletOutOfRangeListeners;
    sf::Int64 playerId;
    sf::Int64 bulletId;
    sf::Vector2f direction;
    bool colliding;
};


#endif //ELKAROYALE_BULLETENTITY_H
