//
// Created by Radoslaw Panuszewski on 1/13/19.
//

#ifndef ELKAROYALE_PLAYERENTITY_H
#define ELKAROYALE_PLAYERENTITY_H


#include <models/Entity.h>
#include <util/functional_typedefs.h>
#include "BulletEntity.h"

using Action = std::function<void()>;
using DirectionConsumer = std::function<void(Direction)>;


class PlayerEntity: public Entity {

public:
    PlayerEntity(UniformGrid *uniformGrid);

    void tryMoveLeft(Action onSuccess = []{});
    void tryMoveRight(Action onSuccess = []{});
    void tryMoveUp(Action onSuccess = []{});
    void tryMoveDown(Action onSuccess = []{});
    void tryMoveLeftUp(DirectionConsumer onSuccess = [](auto d){});
    void tryMoveLeftDown(DirectionConsumer onSuccess = [](auto d){});
    void tryMoveRightUp(DirectionConsumer onSuccess = [](auto d){});
    void tryMoveRightDown(DirectionConsumer onSuccess = [](auto d){});

    void listenOnGotHit(IdConsumer listener);

    sf::Int64 getId() const { return id; }
    std::string getName() const { return name; }
    short getHealthPoints() const { return healthPoints; }
    float getRotation() const { return rotation; }

    void setRotation(float angle);
    void setId(long id) { this->id = id; }
    void setName(const std::string &name) { this->name = name; }
    void setHealthPoints(short healthPoints) { this->healthPoints = healthPoints; }
    void applyDamge(short damaged) { healthPoints -= damaged; }
    void applyHeal(short healed) { healthPoints += healed; }

    void onCollision(Entity *collidingEntity) override;
    void onGotHit(sf::Int64 shooterId);

private:
    static constexpr float PLAYER_STEP = 10.0f;
    static constexpr float PLAYER_DIAGONAL_STEP = static_cast<float>(sqrt(PLAYER_STEP * PLAYER_STEP / 2.f));
    std::vector<IdConsumer> hitListeners;
    sf::Int64 previouslyCollidingBulletId;

    long id;
    std::string name;
    float rotation;
    short healthPoints;
};


#endif //ELKAROYALE_PLAYERENTITY_H
