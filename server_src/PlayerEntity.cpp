//
// Created by Radoslaw Panuszewski on 1/13/19.
//

#include "PlayerEntity.h"
#include "BulletEntity.h"

PlayerEntity::PlayerEntity(UniformGrid *uniformGrid) :
        Entity(sf::Vector2f(70.f, 70.f), uniformGrid)
{
    previouslyCollidingBulletId = -1;
}

void PlayerEntity::onCollision(Entity *collidingEntity) {
    Entity::onCollision(collidingEntity);

    if (BulletEntity *bulletEntity = dynamic_cast<BulletEntity*>(collidingEntity)) {
        if (bulletEntity->getBulletId() == previouslyCollidingBulletId) return;
        previouslyCollidingBulletId = bulletEntity->getBulletId();
        onGotHit(bulletEntity->getPlayerId());
    }
}

void PlayerEntity::onGotHit(sf::Int64 shooterId) {
    for (auto& listener : hitListeners) {
        listener(shooterId);
    }
}

void PlayerEntity::listenOnGotHit(IdConsumer listener) {
    hitListeners.push_back(listener);
}

void PlayerEntity::tryMoveLeft(Action onSuccess) {
    position.x -= PLAYER_STEP;
    if (!checkCollisions())
        onSuccess();
    else
        position.x += PLAYER_STEP;
}

void PlayerEntity::tryMoveRight(Action onSuccess) {
    position.x += PLAYER_STEP;
    if (!checkCollisions())
        onSuccess();
    else
        position.x -= PLAYER_STEP;
}

void PlayerEntity::tryMoveUp(Action onSuccess) {
    position.y -= PLAYER_STEP;
    if (!checkCollisions())
        onSuccess();
    else
        position.y += PLAYER_STEP;
}

void PlayerEntity::tryMoveDown(Action onSuccess) {
    position.y += PLAYER_STEP;
    if (!checkCollisions())
        onSuccess();
    else
        position.y -= PLAYER_STEP;
}

void PlayerEntity::tryMoveLeftUp(DirectionConsumer onSuccess) {
    position.x -= PLAYER_DIAGONAL_STEP;
    position.y -= PLAYER_DIAGONAL_STEP;

    if (!checkCollisions())
        onSuccess(Direction::LEFT_UP);
    else {
        position.x += PLAYER_DIAGONAL_STEP;
        position.y += PLAYER_DIAGONAL_STEP;
        tryMoveLeft([=] { onSuccess(Direction::LEFT); });
        tryMoveUp([=] { onSuccess(Direction::UP); });
    }
}

void PlayerEntity::tryMoveLeftDown(DirectionConsumer onSuccess) {
    position.x -= PLAYER_DIAGONAL_STEP;
    position.y += PLAYER_DIAGONAL_STEP;

    if (!checkCollisions())
        onSuccess(Direction::LEFT_DOWN);
    else {
        position.x += PLAYER_DIAGONAL_STEP;
        position.y -= PLAYER_DIAGONAL_STEP;
        tryMoveLeft([=] { onSuccess(Direction::LEFT); });
        tryMoveDown([=] { onSuccess(Direction::DOWN); });
    }
}

void PlayerEntity::tryMoveRightUp(DirectionConsumer onSuccess) {
    position.x += PLAYER_DIAGONAL_STEP;
    position.y -= PLAYER_DIAGONAL_STEP;

    if (!checkCollisions())
        onSuccess(Direction::RIGHT_UP);
    else {
        position.x -= PLAYER_DIAGONAL_STEP;
        position.y += PLAYER_DIAGONAL_STEP;
        tryMoveRight([=] { onSuccess(Direction::RIGHT); });
        tryMoveUp([=] { onSuccess(Direction::UP); });
    }
}

void PlayerEntity::tryMoveRightDown(DirectionConsumer onSuccess) {
    position.x += PLAYER_DIAGONAL_STEP;
    position.y += PLAYER_DIAGONAL_STEP;

    if (!checkCollisions())
        onSuccess(Direction::RIGHT_DOWN);
    else {
        position.x -= PLAYER_DIAGONAL_STEP;
        position.y -= PLAYER_DIAGONAL_STEP;
        tryMoveRight([=] { onSuccess(Direction::RIGHT); });
        tryMoveDown([=] { onSuccess(Direction::DOWN); });
    }
}

void PlayerEntity::setRotation(float angle) {
    rotation = angle;
}
