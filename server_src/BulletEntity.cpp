//
// Created by Radoslaw Panuszewski on 1/15/19.
//

#include "BulletEntity.h"
#include "PlayerEntity.h"

BulletEntity::BulletEntity(UniformGrid *uniformGrid) :
        Entity(sf::Vector2f(10.f, 10.f), uniformGrid),
        colliding(false)
{}

void BulletEntity::continueFlight() {

    position += direction * 20.f;

    if (position.x < 0 || position.y < 0 ||
        position.x > uniformGrid->getMapSize().x || position.y > uniformGrid->getMapSize().y)
    {
        onOutOfRange();
        return;
    }

    checkCollisions();
}

void BulletEntity::listenOnBulletOutOfRange(IdConsumer listener) {
    bulletOutOfRangeListeners.push_back(listener);
}

void BulletEntity::onCollision(Entity *collidingEntity) {
    
    if (colliding) return;
    colliding = true;
    
    Entity::onCollision(collidingEntity);       

    if (dynamic_cast<PlayerEntity*>(collidingEntity)) {
        onOutOfRange();
    }
}

void BulletEntity::onOutOfRange() {
    for (auto& listener : bulletOutOfRangeListeners) {
        listener(bulletId);
    }
}