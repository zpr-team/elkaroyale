//
// Created by cmanszew on 11.01.19.
//

#ifndef ELKAROYALE_SERVER_H
#define ELKAROYALE_SERVER_H

#include <iostream>
#include <cmath>
#include <list>
#include <SFML/Network.hpp>
#include <models/GameState.h>
#include <queue>
#include <models/HitTimestamp.h>
#include "PlayerEntity.h"
#include "models/GameStateTimestamp.h"


sf::Packet& operator <<(sf::Packet& packet, const GameStateTimestamp& gameStateTimestamp);
sf::Packet& operator <<(sf::Packet& packet, const PlayerTimestamp& playerTimestamp);
sf::Packet& operator <<(sf::Packet& packet, const ShotTimestamp& timestamp);
sf::Packet& operator <<(sf::Packet& packet, const HitTimestamp& hitTimestamp);

sf::Packet& operator >>(sf::Packet& packet, PlayerTimestamp& playerTimestamp);
sf::Packet& operator >>(sf::Packet& packet, ShotTimestamp& timestamp);


using SocketPair = std::pair<std::unique_ptr<sf::TcpSocket>, bool>;
using SocketTriplet = std::tuple<std::unique_ptr<sf::TcpSocket>, bool, sf::Int64>;

enum RQ {SPAWN, DISCONNECT, UPDATE, PING,
    MOVE_LEFT, MOVE_RIGHT, MOVE_UP, MOVE_DOWN, MOVE_LEFT_UP, MOVE_LEFT_DOWN, MOVE_RIGHT_UP, MOVE_RIGHT_DOWN,
    SET_ROTATION, SHOOT, GOT_HIT};


class Server {
public:
    void launch();

private:
    sf::Int64 getId();
    bool handleRequest(SocketTriplet &socket, sf::Packet& packet);
    void spawnPlayer(SocketTriplet &socket, sf::Packet& packet);
    void spawnBullet(SocketTriplet &socket, sf::Packet &packet);
    void disconnectPlayer(SocketTriplet &socket);
    void movePlayer(SocketTriplet &socket, sf::Int32 requestType);
    void rotatePlayer(SocketTriplet &socket, sf::Packet& packet);

    sf::Vector2f generateValidRandomPlayerPosition();
    sf::Color generateRandomColor();
    unsigned long distinctRandom();
    
    PlayerTimestamp makeTimestamp(PlayerEntity *playerEntity);
    GameStateTimestamp makeTimestamp(GameState *gameState);

    void updateSimulation();
    void broadcastGameState();
    void getUserInput();
    void checkClientPings();

    const short PORT = 2048;
    const sf::IpAddress IP = sf::IpAddress::getLocalAddress();
    const sf::Time REFRESH_RATE = sf::milliseconds(1);
    const sf::Time SOCKET_SELECTOR_TIMEOUT = sf::seconds(3);
    const sf::Time CHECK_PINGS_RATE = sf::seconds(1);
    const sf::Time CLIENT_TIMEOUT = sf::seconds(4);
    const float PLAYER_STEP = 10.0f;
    const float PLAYER_DIAGONAL_STEP = static_cast<float>(sqrt(PLAYER_STEP * PLAYER_STEP / 2.f));
    const float GUN_LENGTH = 100.f;
    const std::vector<std::string> colorPresets = { "990000", "006600", "000099", "CC6600", "660066",
                                                    "9933FF", "009999", "99004C", "999900", "003333"};
    bool exit;

    sf::SocketSelector socketSelector;
    sf::Mutex socketsMutex;
    sf::Mutex pingMutex;

    std::mutex playersMutex;
    std::mutex bulletsMutex;

    std::list<SocketTriplet> sockets;
    std::map<sf::Int64, sf::Time> clientPings;
    GameState gameState;
    UniformGrid uniformGrid;

};


#endif //ELKAROYALE_SERVER_H
