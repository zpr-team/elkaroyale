//
// Created by cmanszew on 06.01.19.
//

#include <Server.h>

int main()
{
    Server server;
    server.launch();

    return 0;
}