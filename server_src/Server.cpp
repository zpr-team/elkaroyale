//
// Created by cmanszew on 11.01.19.
//

#include <thread>
#include <SFML/Graphics/Transform.hpp>
#include "Server.h"
#include <chrono>

void Server::launch()
{
    srand(time(0));
    sf::TcpListener listener;

    exit = false;
    gameState.startTime();

    std::thread simulation(&Server::updateSimulation, this);
    std::thread broadcast(&Server::broadcastGameState, this);
    std::thread userInput(&Server::getUserInput, this);
    std::thread clientPings(&Server::checkClientPings, this);

    listener.listen(PORT, IP);
    std::cout << IP.toString() << std::endl;
    socketSelector.add(listener);

    while (!exit) {
        if(socketSelector.wait(SOCKET_SELECTOR_TIMEOUT)) {
            if (socketSelector.isReady(listener)) {
                std::unique_ptr<sf::TcpSocket> socket = std::make_unique<sf::TcpSocket>();
                listener.accept(*socket);
                socketSelector.add(*socket);

                sf::Packet packet;
                sf::Int64 id = getId();
                packet << id;
                socket->send(packet);

                socketsMutex.lock();
                sockets.emplace_back(std::make_tuple(move(socket), false, id));
                socketsMutex.unlock();
                std::cout << "Id: " << id << " connected" << std::endl;
            } else {
                sf::Packet packet;
                socketsMutex.lock();
                for (auto it = sockets.begin(); it != sockets.end(); ++it) {

                    auto& socket = *it;
                    if (socketSelector.isReady(*(std::get<0>(socket)))) {
                        packet.clear();

                        if (std::get<0>(socket)->receive(packet) == sf::Socket::Done)
                            if (!handleRequest(socket, packet))
                                --it;
                    }
                }
                socketsMutex.unlock();
            }
        }
    }
    broadcast.join();
    userInput.join();
    clientPings.join();
}

bool Server::handleRequest(SocketTriplet &socket, sf::Packet &packet)
{
    sf::Int32 requestType;
    packet >> requestType;

    switch (requestType) {
        case RQ::SPAWN:
            spawnPlayer(socket, packet);
            return true;
        case RQ::DISCONNECT:
            disconnectPlayer(socket);
            return false;
        case RQ::MOVE_LEFT:
        case RQ::MOVE_RIGHT:
        case RQ::MOVE_UP:
        case RQ::MOVE_DOWN:
        case RQ::MOVE_LEFT_UP:
        case RQ::MOVE_LEFT_DOWN:
        case RQ::MOVE_RIGHT_UP:
        case RQ::MOVE_RIGHT_DOWN:
            movePlayer(socket, requestType);
            return true;;
        case RQ::SET_ROTATION:
            rotatePlayer(socket, packet);
            return true;
        case RQ::PING:
            clientPings[std::get<2>(socket)] = gameState.getCurrentTime();
            return true;
        case RQ::SHOOT:
            spawnBullet(socket, packet);
            return true;
        default:
            return true;
    }
}

sf::Int64 Server::getId()
{
    static sf::Int64 nextId = 0;
    return nextId++;
}

void Server::spawnPlayer(SocketTriplet &socket, sf::Packet &packet)
{
    sf::Int64 id = std::get<2>(socket);
    std::string playerName;

    packet >> playerName;
    std::cout << "Player: " << playerName << " joins the game\n";

    std::unique_ptr<PlayerEntity> playerEntity = std::make_unique<PlayerEntity>(&uniformGrid);
    playerEntity->setId(id);
    playerEntity->setName(playerName);
    playerEntity->setColor(generateRandomColor());
    playerEntity->setPosition(generateValidRandomPlayerPosition());
    playerEntity->setRotation(0);
    playerEntity->setHealthPoints(100);

    gameState.addPlayer(std::move(playerEntity));

    packet.clear();
    packet << makeTimestamp(gameState.getPlayer(id));
    (std::get<0>(socket))->send(packet);
    std::cout << "Player: " << playerName << " (id: "<< id << ") spawned" << std::endl;
    std::get<1>(socket) = true;
}

void Server::spawnBullet(SocketTriplet &socket, sf::Packet &packet) {

    static sf::Int64 nextBulletId = 0;

    sf::Int64 bulletId = nextBulletId++;
    sf::Int64 playerId = std::get<2>(socket);

    sf::Vector2f startPosition;
    sf::Vector2f direction;

    packet >> startPosition.x >> startPosition.y >> direction.x >> direction.y;

    std::unique_ptr<BulletEntity> bulletEntity = std::make_unique<BulletEntity>(&uniformGrid);

    bulletEntity->setBulletId(bulletId);
    bulletEntity->setPlayerId(playerId);
    bulletEntity->setPosition(startPosition);
    bulletEntity->setDirection(direction);

    bulletsMutex.lock();
    gameState.addBullet(std::move(bulletEntity));
    bulletsMutex.unlock();

    ShotTimestamp shotTimestamp;
    shotTimestamp.position = startPosition;
    shotTimestamp.direction = direction;
    shotTimestamp.playerId = playerId;

    packet.clear();
    packet << RQ::SHOOT << shotTimestamp;
    for (const auto &socketIt : sockets)
        if (std::get<1>(socketIt))
            (std::get<0>(socketIt))->send(packet);
}

void Server::disconnectPlayer(SocketTriplet &socket)
{
    sf::Int64 id = std::get<2>(socket);

    gameState.deletePlayer(id);
    clientPings.erase(id);
    socketSelector.remove(*(std::get<0>(socket)));
    std::get<0>(socket)->disconnect();
    sockets.remove(socket);

    sf::Packet packet;
    packet << RQ::DISCONNECT << id;
    for (const auto &socketIt : sockets)
        if (std::get<1>(socketIt))
            (std::get<0>(socketIt))->send(packet);
}

void Server::movePlayer(SocketTriplet &socket, sf::Int32 requestType)
{
    sf::Int64 id = std::get<2>(socket);
    switch (requestType) {
        case RQ::MOVE_LEFT:
            gameState.getPlayer(id)->tryMoveLeft();
            break;
        case RQ::MOVE_RIGHT:
            gameState.getPlayer(id)->tryMoveRight();
            break;
        case RQ::MOVE_UP:
            gameState.getPlayer(id)->tryMoveUp();
            break;
        case RQ::MOVE_DOWN:
            gameState.getPlayer(id)->tryMoveDown();
            break;
        case RQ::MOVE_LEFT_UP:
            gameState.getPlayer(id)->tryMoveLeftUp();
            break;
        case RQ::MOVE_LEFT_DOWN:
            gameState.getPlayer(id)->tryMoveLeftDown();
            break;
        case RQ::MOVE_RIGHT_UP:
            gameState.getPlayer(id)->tryMoveRightUp();
            break;
        case RQ::MOVE_RIGHT_DOWN:
            gameState.getPlayer(id)->tryMoveRightDown();
            break;
        default:
            break;
    }
}

void Server::rotatePlayer(SocketTriplet &socket, sf::Packet& packet)
{
    sf::Int64 id = std::get<2>(socket);
    float rotation;
    packet >> rotation;
    gameState.getPlayer(id)->setRotation(rotation);
}

void Server::updateSimulation() {

    while (!exit) {
        std::chrono::system_clock::time_point endOfFrame = std::chrono::system_clock::now() + std::chrono::milliseconds(1000/60);

        gameState.deleteExpiringBullets();

        bulletsMutex.lock();
        for (auto& bulletEntityPair : gameState.getBullets()) {
            bulletEntityPair.second->continueFlight();
        }
        bulletsMutex.unlock();

        std::this_thread::sleep_until(endOfFrame);
    }
}

PlayerTimestamp Server::makeTimestamp(PlayerEntity *playerEntity) {

    PlayerTimestamp timestamp;
    timestamp.id = playerEntity->getId();
    timestamp.name = playerEntity->getName();
    timestamp.color = playerEntity->getColor();
    timestamp.position = playerEntity->getPosition();
    timestamp.rotation = playerEntity->getRotation();
    timestamp.healthPoints = playerEntity->getHealthPoints();
    timestamp.timeMillis = gameState.getCurrentTime().asMilliseconds();
    return timestamp;
}

GameStateTimestamp Server::makeTimestamp(GameState *gameState) {

    GameStateTimestamp gameStateTimestamp {};

    for (auto& playerPair : gameState->getPlayers()) {
        auto playerTimestamp = makeTimestamp(playerPair.second.get());
        gameStateTimestamp.players[playerTimestamp.id] = playerTimestamp;
    }

    return gameStateTimestamp;
}

void Server::broadcastGameState()
{
    sf::Packet packet;
    while (!exit) {
        packet.clear();
        packet << RQ::UPDATE << makeTimestamp(&gameState);
        socketsMutex.lock();
        for (const auto &socket : sockets)
            if (std::get<1>(socket))
                (std::get<0>(socket))->send(packet);
        socketsMutex.unlock();
        sf::sleep(REFRESH_RATE);
    }
}

void Server::getUserInput()
{
    std::string command;
    while (!exit) {
        std::cin >> command;
        std::cout << "Command entered: " << command << std::endl;

        if (command == "exit")
            exit = true;
    }
}

void Server::checkClientPings()
{
    sf::Time currentTime, timeDiff;
    while (!exit) {
        currentTime = gameState.getCurrentTime();
        socketsMutex.lock();
        for (auto &pings : clientPings) {
            timeDiff = currentTime - pings.second;
            if (timeDiff > CLIENT_TIMEOUT) {

                auto it = std::find_if(sockets.begin(), sockets.end(), [&](const SocketTriplet &e)
                { return std::get<2>(e) == pings.first; });

                disconnectPlayer(*it);
            }
        }
        socketsMutex.unlock();
        sf::sleep(CHECK_PINGS_RATE);
    }
}

sf::Vector2f Server::generateValidRandomPlayerPosition() {
    sf::Vector2f position;
    PlayerEntity playerEntity(&uniformGrid);
    do {
        position = sf::Vector2f(rand() % 800, rand() % 600);
        playerEntity.setPosition(position);
    } while (playerEntity.checkCollisions());
    return position;
}

sf::Color Server::generateRandomColor() {
    std::string colorHex = colorPresets[distinctRandom()];
    int r, g, b;
    sscanf(colorHex.c_str(), "%02x%02x%02x", &r, &g, &b);
    return sf::Color(r, g, b);
}

unsigned long Server::distinctRandom() {
    static std::set<unsigned long> indexes;
    if (indexes.size() == colorPresets.size()) indexes.clear();
    unsigned long index;
    do {
        index = rand() % colorPresets.size();
    } while (indexes.count(index) != 0);
    indexes.insert(index);
    return index;
}

sf::Packet& operator >>(sf::Packet& packet, PlayerTimestamp& playerTimestamp)
{
    packet >> playerTimestamp.id >> playerTimestamp.name >> playerTimestamp.healthPoints
           >> playerTimestamp.color.r >> playerTimestamp.color.g >> playerTimestamp.color.b;
    packet >> playerTimestamp.position.x >> playerTimestamp.position.y >> playerTimestamp.rotation
           >> playerTimestamp.timeMillis;
    return packet;
}

sf::Packet& operator <<(sf::Packet& packet, const PlayerTimestamp& playerTimestamp)
{
    packet << playerTimestamp.id << playerTimestamp.name << playerTimestamp.healthPoints
           << playerTimestamp.color.r << playerTimestamp.color.g << playerTimestamp.color.b;
    packet << playerTimestamp.position.x << playerTimestamp.position.y << playerTimestamp.rotation
           << playerTimestamp.timeMillis;
    return packet;
}

sf::Packet& operator <<(sf::Packet& packet, const GameStateTimestamp& gameStateTimestamp)
{
    sf::Int64 playerCount = static_cast<sf::Int64>(gameStateTimestamp.players.size());
    packet << playerCount;
    for (const auto& player : gameStateTimestamp.players) {
        packet << player.second;
    }
    return packet;
}

sf::Packet& operator >>(sf::Packet& packet, ShotTimestamp& timestamp) {

    packet >> timestamp.position.x >> timestamp.position.y >> timestamp.direction.x >> timestamp.direction.y >> timestamp.playerId >> timestamp.timeMillis;
    return packet;
}


sf::Packet& operator <<(sf::Packet& packet, const ShotTimestamp& timestamp) {

    packet << timestamp.position.x << timestamp.position.y << timestamp.direction.x << timestamp.direction.y << timestamp.playerId << timestamp.timeMillis;
    return packet;
}


sf::Packet& operator <<(sf::Packet& packet, const HitTimestamp& hitTimestamp) {
    packet << hitTimestamp.shooterId << hitTimestamp.victimId << hitTimestamp.timeMillis;
    return packet;
}
