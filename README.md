# ElkaRoyale #

Simple multiplayer 2D battle-royale game, created during the course of Advanced Programming in C++ at Warsaw University of Technology.

![Alt text](https://i.ibb.co/7tTdHBn/Elka-Royale-screenshoot.png)